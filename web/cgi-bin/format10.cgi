#!/usr/bin/ruby
# encoding: utf-8

$:.unshift('../rlibs')
$:.unshift('/srv/udd.debian.org/udd/rlibs')
require 'udd-db'
require 'pp'
require 'cgi'
require 'time'
require 'yaml'

STDERR.reopen(STDOUT)
puts "Content-type: text/html\n\n"

tstart = Time::now
DB = Sequel.connect(UDD_GUEST)
DB["SET statement_timeout TO 90000"]
DB.extension :pg_array

def dbget(q, *args)
  return DB.fetch(q, *args).all.sym2str
end

TRIVIAL_FIX=%w{
4g8 abootimg aconnectgui acpi aft alsamixergui amideco appconfig ascdc asmail
asmix aspell-el avfs aview avrp awardeco aylet baycomusb beav bfbtester bindfs
binstats blop catdvi cbmplugs cconv cd-circleprint cdde cereal chase chrpath
ciphersaber citadel-client cl-log cl-regex coco-cpp coco-cs coco-java code2html
codegroup compartment console-cyrillic coolmail cpipe crack-attack crypt++el
cvs cycfx2prog daemontools dbix-easy-perl dbus-sharp-glib debaux debfoster
dictconv dictem ding-libs dirdiff dist dmitry dns323-firmware-tools dnsmasq
docbook-website dutch dv4l dvdtape edict-el eldav fake famfamfam-flag
festlex-poslex festvox-kallpc16k festvox-kallpc8k festvox-kdlpc16k
festvox-kdlpc8k flvstreamer fortunes-bofh-excuses fortunes-it freebirth
freetable freetds funnelweb-doc fuse-umfuse-ext2 fwanalog g2p-sk gav-themes
gcc-3.3 gkrellm-reminder gkrellm-thinkbat gkrellm-xkb glbsp glktermw glslang
glurp glw gmemusage gnomediaicons gplaycli gss-ntlmssp gtkterm gwaterfall
gworldclock hsqldb1.8.0 ifrench-gut imaprowl imgsizer imgvtopgm impose+ inn
intel2gas ipv6calc its-playback-time jargon jdresolve jgraph jtex-base judy
kawari8 kelbt knews lakai libalgorithm-dependency-perl
libanyevent-serialize-perl libapache2-mod-log-slow libapache-gallery-perl
libaudio-scrobbler-perl libbiblio-isis-perl libcitadel libclass-csv-perl
libclass-pluggable-perl libcrypt-smbhash-perl libdansguardian-perl
libdata-javascript-anon-perl libdatapager-perl libdata-validate-domain-perl
libdbd-sybase-perl libdbix-dr-perl libdevice-usb-pcsensor-hidtemper-perl
libdmx libebook-tools-perl libemail-foldertype-perl libexpect-perl
libfile-chdir-perl libfile-searchpath-perl libfontenc libformula libfs libglu
libgraphics-colornames-perl libgraphics-colorobject-perl
libhtml-element-extended-perl libhtml-popuptreeselect-perl libice
libimage-metadata-jpeg-perl libjcode-pm-perl libjlayer-java libjmac-java
liblog-dispatch-filerotate-perl libmodem-vgetty-perl libmp4-info-perl
libnbcompat libnet-finger-perl libnet-proxy-perl libpciaccess
libperlmenu-perl libpthread-stubs libromana-perligata-perl libropkg-perl
libsendmail-pmilter-perl libsm libtemplate-plugin-cycle-perl
libtemplate-plugin-utf8decode-perl libtext-aligner-perl libtext-chasen-perl
libtext-table-perl libtext-unaccent-perl libtie-shadowhash-perl
libtimezonemap libtree-multinode-perl libunibreak libunity libuser-perl
libx11 libxau libxaw libxcb libxcomposite libxdmcp libxext libxfixes libxfont
libxi libxinerama libxkbfile libxml-dumper-perl libxml-rss-feed-perl libxmu
libxpm libxpresent libxrandr libxrender libxshmfence libxss libxt libxv
libxvmc libxxf86dga libxxf86vm libyaml-shell-perl lice linklint loadwatch
logtool lsmbox m16c-flash mailagent make-dfsg makepasswd makepatch makexvpics
manpages-tr mapivi markdown mdm mesa-demos mhonarc midge moblin-gtk-engine
mpclib3 mrtg-ping-probe msr-tools mtree-netbsd ng-utils nini nlkt nomarch
norwegian nstreams ocamlcreal ocaml-getopt ocaml-magic ocaml-shout
openoffice.org-en-au opensp openuniverse osspsa pandora-build pcaputils pccts
pcre2 pdf2svg pfqueue pgreplay phnxdeco pidgin-awayonlock pixelize pkg-config
pmw pngmeta pngnq postmark powerman prototypejs proxsmtp pscan psgml
pytest-multihost pytest-sourceorder python-hglib python-pam qprint qwo
randtype redet regionset rsbackup ruby-bsearch sanitizer sbox-dtc
scriptaculous searchandrescue searchandrescue-data sgrep sipcalc speex
spirv-headers src2tex sreview ssh-askpass-fullscreen stx2any sunxi-tools
svgtune sylseg-sk tablix2 tcsh tetrinetx tkcvs tolua tor transfermii tua
tuxtype twm ufiformat umview urlview uzbek-wordlist vanessa-adt
vanessa-logger vile vm watchcatd watchdog wavesurfer wayland weston wily
wm-icons wmtemp xauth xcb-proto xcolors xcursor-themes xdm xfaces xft xinit
xinput xjdic xkeyboard-config xless xmix xorg-docs xorgproto xorg-server
xorg-sgml-doctools xringd xserver-xorg-input-aiptek xserver-xorg-input-evdev
xserver-xorg-input-libinput xserver-xorg-video-amdgpu xserver-xorg-video-ati
xserver-xorg-video-cirrus xserver-xorg-video-dummy xserver-xorg-video-fbdev
xserver-xorg-video-intel xserver-xorg-video-mga xserver-xorg-video-neomagic
xserver-xorg-video-nouveau xserver-xorg-video-openchrome
xserver-xorg-video-qxl xserver-xorg-video-r128 xserver-xorg-video-savage
xserver-xorg-video-siliconmotion xserver-xorg-video-sisusb
xserver-xorg-video-tdfx xserver-xorg-video-vesa xserver-xorg-video-vmware
xsettings-kde xtide-coastline xtrans yapps2 yaret zfsnap zmakebas
}

dbget("CREATE TEMPORARY TABLE trivial_fix(source text)")
s = TRIVIAL_FIX.map { |p| "('#{p}')" }.join(', ')
dbget("INSERT INTO trivial_fix VALUES #{s}")

dbget <<-EOF
CREATE TEMPORARY VIEW format10 AS

with sources10 as (
         select source, version, maintainer_email,
                vcs_url != '' as vcs,
                vcs_url,
                vcs_browser
         from sources_uniq where format='1.0' and release='bookworm'
         ),     
lmu as (
         select source, max(date) as last_maintainer_upload from upload_history
                where nmu = false
                group by source
                order by 2
       ),      
patchsys as (
                select distinct package as source, information as patch_system
                from lintian
                where tag='patch-system'
            ),
implicit as (
                select distinct package as source, true as implicit
                from lintian
                where tag='missing-debian-source-format'
            ),
directch as (
                select distinct package as source, information != '' as direct_changes
                from lintian
                where tag='direct-changes-in-diff-but-no-patch-system'
            ), 
directchps as (
                select distinct package as source, information != '' as direct_changes_and_patch_system
                from lintian
                where tag='patch-system-but-direct-changes-in-diff'
            ),
pc as (  
                select source, insts as popcon
                from popcon_src
      ),
vcsstatus as (
                select source, status as vcs_status
                from vcswatch
),
key as (
        select source, reason != '' as key_package
        from key_packages
),
trivial_fix as (
        select source, true as trivial_fix
        from trivial_fix
),
native as (
        SELECT source, not(files ~ '.orig.tar.gz') as native_pkg FROM sources_uniq where release='bookworm'
),
bugs as (
        SELECT source, max(id) as bug_id, max(status) as bug_status, max(last_modified) as bug_last_modified from bugs_usertags inner join bugs using (id) where email='lucas@debian.org' and tag='format1.0' group by source
)

select source, version, coalesce(patch_system, 'none') as patch_system,
               coalesce(direct_changes or direct_changes_and_patch_system, false) as direct_changes,
               coalesce(implicit, false) as implicit,
               coalesce(key_package, false) as key_package,
               coalesce(vcs, false) as vcs,
               coalesce(trivial_fix, false) as trivial_fix,
               native_pkg,
               maintainer_email = 'debian-x@lists.debian.org' as debian_x,
               vcs_status,
               last_maintainer_upload,
               popcon,
               vcs_url,
               bug_id,
               bug_status,
               bug_last_modified
from sources10  
left join lmu using (source)
left join patchsys using (source)
left join implicit using (source)
left join directch using (source) 
left join directchps using (source) 
left join key using (source)
left join pc using (source)
left join vcsstatus using (source)
left join trivial_fix using (source)
left join native using (source)
left join bugs using (source)
order by 2,3,4,5,6;
EOF

puts <<-EOF

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Source packages using the 1.0 source format</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<!-- Datatables -->
<script type="text/javascript" charset="utf-8" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>

<style>
table.table-reducedrowheight td, th { padding: 2px 2px !important;}
</style>
</head>
<body>
<div class="container-fluid">
EOF

def pkgtable(id, rows)
puts <<-EOF
<div class="table-responsive"><div class='container-fluid'>
   <table id="#{id}" class="table table-bordered table-striped table-hover table-sm text-center table-reducedrowheight">
     <thead>
       <tr>
         <th class="text-center">Source</th>
         <th class="text-center">Patch system</th>
         <th class="text-center">Direct changes</th>
         <th class="text-center">Key package</th>
         <th class="text-center">VCS</th>
         <th class="text-center">Trivial fix</th>
         <th class="text-center">Native</th>
         <th class="text-center">Debian X</th>
         <th class="text-center">Implicit</th>
         <th class="text-center">VCS status</th>
         <th class="text-center">Last maintainer upload</th>
         <th class="text-center">Popcon</th>
         <th class="text-center">VCS Url</th>
         <th class="text-center">Bug</th>
         <th class="text-center">Bug status</th>
         <th class="text-center">Bug modified</th>
       </tr>
     </thead>
     <tfoot style="display: table-header-group;">
     <tr>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
       <td><input type="text" placeholder="" size="2" style="width:100%;" /></td>
     </tr>
     </tfoot>
     <tbody>
     EOF
     rows.each do |r|
       puts <<-EOF
       <tr>
         <td class="text-nowrap"><a href="https://tracker.debian.org/pkg/#{r['source']}">#{r['source']}</a<</td>
         <td class="text-nowrap">#{r['patch_system']}</td>
         <td class="text-nowrap">#{r['direct_changes'] ? 'yes' : 'no'}</td>
         <td class="text-nowrap">#{r['key_package'] ? 'yes' : 'no'}</td>
         <td class="text-nowrap">#{r['vcs'] ? 'yes' : 'no'}</td>
         <td class="text-nowrap">#{r['trivial_fix'] ? 'yes' : 'no'}</td>
         <td class="text-nowrap">#{r['native_pkg'] ? 'yes' : 'no'}</td>
         <td class="text-nowrap">#{r['debian_x'] ? 'yes' : 'no'}</td>
         <td class="text-nowrap">#{r['implicit'] ? 'yes' : 'no'}</td>
         <td class="text-nowrap"><a href="https://qa.debian.org/cgi-bin/vcswatch?package=#{r['source']}">#{r['vcs_status']}</a></td>
         <td class="text-nowrap">#{r['last_maintainer_upload'].to_date}</td>
         <td class="text-nowrap">#{r['popcon']}</td>
         <td class="text-nowrap">#{r['vcs_url']}</td>
         EOF
         if r['bug_id']
           puts <<-EOF
         <td class="text-nowrap"><a href="https://bugs.debian.org/#{r['bug_id']}">##{r['bug_id']}</a></td>
         <td class="text-nowrap">#{r['bug_status']}</td>
         <td class="text-nowrap">#{r['bug_last_modified'].to_date}</td>
           EOF
         else
           puts <<-EOF
         <td class="text-nowrap"></td>
         <td class="text-nowrap"></td>
         <td class="text-nowrap"></td>
           EOF
         end
         puts <<-EOF
       </tr>
       EOF
     end
     puts <<-EOF
     </tbody>
     </table>
     </div>
     </div>
     EOF
end

puts <<-EOF
<h1>Source packages using the 1.0 format</h1>

<p><a href="https://udd.debian.org/bugs/?release=sid&merged=ign&fnewerval=7&flastmodval=7&fusertag=only&fusertagtag=format1.0&fusertaguser=lucas%40debian.org&allbugs=1&cseverity=1&ckeypackage=1&ctags=1&caffected=1&cmissingbuilds=1&clastupload=1&cwhykey=1&sortby=id&sorto=asc&format=html#results">List of bugs</a></p>

<h2>Packages set 1.1: criteria = no vcs-based workflow AND key package</h2>
<p>The exact criteria is <code>not (debian_x or (vcs and vcs_status != 'ERROR' and direct_changes)) and key_package</code>, with "no direct changes in diff if maintained in a VCS" being an approximation.</p>
EOF
rows = dbget("select * from format10
where not (debian_x or (vcs and vcs_status != 'ERROR' and direct_changes))
      and key_package")
pkgtable('table1', rows)

=begin
puts "<pre>"
rows.each do |r|
  puts "#{r['source']} #{r['version']}"
end
puts "</pre>"
=end

puts <<-EOF
<h2>Packages set 1.2: criteria = no vcs-based workflow AND not key package</h2>
<p>The exact criteria is <code>not (debian_x or (vcs and vcs_status != 'ERROR' and direct_changes)) and not key_package</code></p>
EOF
rows = dbget("select * from format10
where not (debian_x or (vcs and vcs_status != 'ERROR' and direct_changes))
      and not key_package")
pkgtable('table2', rows)

=begin
puts "<pre>"
rows.each do |r|
  puts "#{r['source']} #{r['version']} #{r['native_pkg']} #{r['trivial_fix']}"
end
puts "</pre>"
=end


puts <<-EOF
<h2>Packages set 2: criteria = maintained in an active VCS (or by the Debian X team)</h2>
EOF
rows = dbget("select * from format10
where (debian_x or (vcs and vcs_status != 'ERROR' and direct_changes))")
pkgtable('table0', rows)

src_testing = dbget("select count(distinct source) from sources_uniq where release='bookworm'").first.values.first
src_testing_30_native = dbget("select count(distinct source) from sources_uniq where release='bookworm' and format='3.0 (native)'").first.values.first
src_testing_30_quilt = dbget("select count(distinct source) from sources_uniq where release='bookworm' and format='3.0 (quilt)'").first.values.first
src_testing_10 = dbget("select count(distinct source) from sources_uniq where release='bookworm' and format='1.0'").first.values.first
declaring = dbget("select count(distinct source) from sources_uniq where release='bookworm' and format='1.0' and source not in (select package from lintian where package_type='source' and tag='missing-debian-source-format')").first.values.first
maint_list = "'debian-x@lists.debian.org', 'ijackson@chiark.greenend.org.uk', 'tfheen@debian.org', 'az@debian.org'"
maint = dbget("select count(distinct source) from sources_uniq where release='bookworm' and format='1.0' and maintainer_email in (#{maint_list}) or source in (select source from uploaders where release='bookworm' and email in (#{maint_list}))").first.values.first
declmaint = dbget("select count(distinct source)
                   from sources_uniq where release='bookworm' and format='1.0' and (
                      source not in (select package from lintian where package_type='source' and tag='missing-debian-source-format')
                        or
                      maintainer_email in (#{maint_list})
                        or
                      source in (select source from uploaders where release='bookworm' and email in (#{maint_list})))").first.values.first

puts <<-EOF

<h2>Statistics</h2>
<ul>
<li>Source packages in testing: #{src_testing}
<li>Source packages in testing using 3.0 (native): #{src_testing_30_native} (#{sprintf("%.1f", src_testing_30_native.to_f*100/src_testing)}%)
<li>Source packages in testing using 3.0 (quilt): #{src_testing_30_quilt} (#{sprintf("%.1f", src_testing_30_quilt.to_f*100/src_testing)}%)
<li>Source packages in testing using 1.0: #{src_testing_10} (#{sprintf("%.1f", src_testing_10.to_f*100/src_testing)}%)
<ul>
<li>... with explicit debian/source/format: #{declaring}
<li>... maintained or co-maintained by Debian X, iwj, tfheen, az: #{maint}
<li>... with explicit debian/source/format OR maintained or co-maintained by Debian X, iwj, tfheen, az: #{declmaint} (#{sprintf("%.1f", declmaint.to_f*100/src_testing)}%)
</ul>
</ul>
EOF



puts <<-EOF
  <script type="text/javascript" class="init">
$(document).ready(function() {
EOF
%w{table0 table1 table2}.each do |t|
  puts <<-EOF
    $('##{t}').DataTable(
         { "bPaginate": false,
           "order": []
         }
    ).columns().every( function () {
      var that = this;

      $( 'input', this.footer() ).on( 'keyup change', function () {
        if ( that.search() !== this.value ) {
          that
            .search( this.value )
            .draw();
        }
      } );
    } );
    EOF
end
puts <<-EOF
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})

});
</script>
</div>
</body>
</html>
EOF
