#!/usr/bin/ruby

$:.unshift('../../rlibs')
require 'udd-db'
require 'pp'

puts "Content-type: text/html\n\n"

puts <<-EOF
<html>
<head>
<title>WNPP checks</title>
</head>
<body>
<h1>WNPP checks</h1>
EOF

DB = Sequel.connect(UDD_GUEST)

def dbget(q, *args)
  return DB.fetch(q, *args).all.sym2str
end

# get list of WNPP bugs, in a rather tolerant way
rows = dbget("SELECT id, SUBSTRING(title from '^([A-Z]{1,3}):.*') as type, SUBSTRING(title from '^[A-Z]{1,3}:[ ]*([^ ]+)') as source, title FROM bugs WHERE package='wnpp' AND status!='done' and id not in (select id from bugs_merged_with where id > merged_with) order by id;")

sources = dbget("SELECT DISTINCT source from sources_uniq where release in ('sid', 'experimental') order by source")

sources = sources.hash_values.map { |s| s.first }

puts "<h2>RFP/ITP for packaged software</h2>"
puts "<p>Should probably be closed.</p>"
puts "<ul>"
rows.each do |r|
  next if not ['RFP', 'ITP'].include?(r['type'])
  next if not sources.include?(r['source'])
  puts "<li>#{r['source']}: <a href=\"https://bugs.debian.org/#{r['id']}\">##{r['id']} (#{r['type']})</a> <a href=\"https://tracker.debian.org/#{r['source']}\">tracker</a></li>"
end
puts "</ul>"

puts "<h2>ITA/RFA/RFH/O for packages not in Debian</h2>"
puts "<p>Should probably be closed.</p>"
puts "<ul>"
rows.each do |r|
  next if not ['ITA', 'RFA', 'RFH', 'O'].include?(r['type'])
  next if sources.include?(r['source'])
  puts "<li>#{r['source']}: <a href=\"https://bugs.debian.org/#{r['id']}\">##{r['id']} (#{r['type']})</a> <a href=\"https://tracker.debian.org/#{r['source']}\">tracker</a></li>"
end
puts "</ul>"

puts "<h2>Packages with more than one WNPP bug</h2>"
puts "<p>Should probably be merged.</p>"
puts "<ul>"
rows.group_by { |r| r['source'] }.each_pair do |src, bugs|
  next if bugs.length < 2
  puts "<li>#{src} (<a href=\"https://tracker.debian.org/#{src}\">tracker</a>):<ul>"
  bugs.each do |r|
    puts "<li><a href=\"https://bugs.debian.org/#{r['id']}\">##{r['id']} (#{r['type']})</a> #{r['title']}</li>"
  end
  puts "</ul></li>"
end
puts "</ul>"


puts "<h2>WNPP bugs not following defined format</h2>"
puts "<p>Should probably be retitled accordingly.</p>"
bugs = dbget("select * from wnpp order by id")

retitles1 = []
list1 = []
retitles2 = []
list2 = []
bugs.each do |r|
  if not r['title'] =~ /^(RFP|ITP|O|ITA|RFA|RFH): ([^ ]+) -- (.+)$/
    if r['title'] =~ /^(RFP|ITP|O|ITA|RFA|RFH): ([^ ]+)$/
      list1 << "<li><a href=\"https://bugs.debian.org/#{r['id']}\">##{r['id']}</a>: #{r['title']}</li>"
      retitles1 << "bts retitle #{r['id']} #{r['title']}"
    else
      list2 << "<li><a href=\"https://bugs.debian.org/#{r['id']}\">##{r['id']}</a>: #{r['title']}</li>"
      retitles2 << "bts retitle #{r['id']} #{r['title']}"
    end
  end
end
puts "<ul>"
puts list2.join("\n")
puts "</ul>"
puts "As a list of bts commands:<pre>"
puts retitles2.join("\n")
puts "</pre>"

puts "<h3>Only missing description:</h3>"
puts "<ul>"
puts list1.join("\n")
puts "</ul>"
puts "As a list of bts commands:<pre>"
puts retitles1.join("\n")
puts "</pre>"

puts "<h2>Packages maintained by packages@qa.debian.org without a corresponding ITA or O bug</h2>"
puts "<p>Also listing the first QA upload.</p>"
puts "<p>A WNPP bug should probably be opened.</p>"
ita_o = rows.select { |r| ['ITA', 'O'].include?(r['type']) }.map { |r| r['source'] }

qasrcs = dbget("select source from sources_uniq where release in ('sid', 'experimental') and maintainer_email = 'packages@qa.debian.org' order by source")
puts "<ul>"
qasrcs.each do |s|
  src = s['source']
  next if ita_o.include?(src)
  last = dbget("select * from upload_history where source='#{src}' and maintainer LIKE '%packages@qa.debian.org%' order by date asc limit 1")
  if last.length == 1
    last = last.first
    s = "(since #{last['version']} ; #{last['date']} ; #{last['changed_by_name']} &lt;#{last['changed_by_email']}&gt;)"
  else
    s = ""
  end
  puts "<li>#{src}: <a href=\"https://tracker.debian.org/#{src}\">tracker</a> #{s}</li>"
end
puts "</ul>"

puts "<h2>ITP and ITA without owners</h2>"
puts "<p>A suitable owner should be determined.</p>"
itpita = rows.select { |r| ['ITA', 'ITP'].include?(r['type']) }.map { |r| r['id'] }.map { |s| "'#{s}'"}.join(', ')
bugs = dbget("select * from bugs where id in (#{itpita}) and owner = '' order by id")
puts "<ul>"
bugs.each do |r|
  puts "<li><a href=\"https://bugs.debian.org/#{r['id']}\">##{r['id']}</a>: #{r['title']}</li>"
end
puts "</ul>"

puts "</body></html>"
