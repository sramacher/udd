--
-- PostgreSQL database dump
--

-- Dumped from database version 11.12 (Debian 11.12-0+deb10u1)
-- Dumped by pg_dump version 11.12 (Debian 11.12-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: history; Type: SCHEMA; Schema: -; Owner: udd
--

CREATE SCHEMA history;


ALTER SCHEMA history OWNER TO udd;

--
-- Name: debversion; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS debversion WITH SCHEMA public;


--
-- Name: EXTENSION debversion; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION debversion IS 'A Debian version number data type';


--
-- Name: bugs_severity; Type: TYPE; Schema: public; Owner: udd
--

CREATE TYPE public.bugs_severity AS ENUM (
    'fixed',
    'wishlist',
    'minor',
    'normal',
    'important',
    'serious',
    'grave',
    'critical'
);


ALTER TYPE public.bugs_severity OWNER TO udd;

--
-- Name: dehs_status; Type: TYPE; Schema: public; Owner: udd
--

CREATE TYPE public.dehs_status AS ENUM (
    'error',
    'uptodate',
    'outdated',
    'newer-in-debian'
);


ALTER TYPE public.dehs_status OWNER TO udd;

--
-- Name: ftp_autoreject_level; Type: TYPE; Schema: public; Owner: udd
--

CREATE TYPE public.ftp_autoreject_level AS ENUM (
    'fatal',
    'nonfatal'
);


ALTER TYPE public.ftp_autoreject_level OWNER TO udd;

--
-- Name: ftp_autoreject_type; Type: TYPE; Schema: public; Owner: udd
--

CREATE TYPE public.ftp_autoreject_type AS ENUM (
    'lintian'
);


ALTER TYPE public.ftp_autoreject_type OWNER TO udd;

--
-- Name: lintian_tagtype; Type: TYPE; Schema: public; Owner: udd
--

CREATE TYPE public.lintian_tagtype AS ENUM (
    'experimental',
    'overridden',
    'pedantic',
    'information',
    'warning',
    'error',
    'classification'
);


ALTER TYPE public.lintian_tagtype OWNER TO udd;

--
-- Name: security_issues_releases_status; Type: TYPE; Schema: public; Owner: udd
--

CREATE TYPE public.security_issues_releases_status AS ENUM (
    'open',
    'resolved',
    'undetermined'
);


ALTER TYPE public.security_issues_releases_status OWNER TO udd;

--
-- Name: security_issues_scope; Type: TYPE; Schema: public; Owner: udd
--

CREATE TYPE public.security_issues_scope AS ENUM (
    'local',
    'remote'
);


ALTER TYPE public.security_issues_scope OWNER TO udd;

--
-- Name: array_sort(anyarray); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.array_sort(anyarray) RETURNS anyarray
    LANGUAGE sql
    AS $_$
SELECT ARRAY(
    SELECT $1[s.i] AS "foo"
    FROM
        generate_series(array_lower($1,1), array_upper($1,1)) AS s(i)
    ORDER BY foo
);
$_$;


ALTER FUNCTION public.array_sort(anyarray) OWNER TO udd;

--
-- Name: bibtex(); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.bibtex() RETURNS SETOF text
    LANGUAGE sql
    AS $$
  SELECT DISTINCT
         CASE WHEN bibjournal.value IS NULL AND bibin.value IS NOT NULL AND bibpublisher.value IS NOT NULL THEN '@Book{' || bibkey.value
              ELSE CASE WHEN bibauthor.value IS NULL OR bibjournal.value IS NULL THEN '@Misc{'|| bibkey.value ||
                   CASE WHEN bibauthor.value IS NULL THEN E',\n  Key     = "' || bibkey.value || '"' ELSE '' END -- without author we need a sorting key
              ELSE '@Article{' || bibkey.value END END  ||
            CASE WHEN bibauthor.value  IS NOT NULL THEN E',\n  Author  = {' || bibauthor.value  || '}' ELSE '' END ||
            CASE WHEN bibtitle.value   IS NOT NULL THEN E',\n  Title   = "{' || 
                  replace(replace(replace(replace(bibtitle.value,
                        '_', E'\\_'),            --
                        '%', E'\\%'),            --
                        '&', E'\\&'),            --
                        E'\xe2\x80\x89', E'\\,') -- TeX syntax for '_' and UTF-8 "thin space"
                                               -- see http://www.utf8-chartable.de/unicode-utf8-table.pl?start=8192&number=128&utf8=string-literal
                   || '}"'
                 ELSE '' END ||
            CASE WHEN bibbooktitle.value IS NOT NULL THEN E',\n  Booktitle = "{' || bibbooktitle.value || '}"' ELSE '' END ||
            CASE WHEN bibyear.value    IS NOT NULL THEN E',\n  Year    = {' || bibyear.value    || '}' ELSE '' END ||
            CASE WHEN bibmonth.value   IS NOT NULL THEN E',\n  Month   = {' || bibmonth.value   || '}' ELSE '' END ||
            CASE WHEN bibjournal.value IS NOT NULL THEN E',\n  Journal = {' || replace(bibjournal.value, '&', E'\\&') || '}' ELSE '' END ||
            CASE WHEN bibaddress.value IS NOT NULL THEN E',\n  Address = {' || bibaddress.value || '}' ELSE '' END ||
            CASE WHEN bibpublisher.value IS NOT NULL THEN E',\n  Publisher = {' || bibpublisher.value || '}' ELSE '' END ||
            CASE WHEN bibvolume.value  IS NOT NULL THEN E',\n  Volume  = {' || bibvolume.value  || '}' ELSE '' END ||
            CASE WHEN bibnumber.value  IS NOT NULL THEN E',\n  Number  = {' || bibnumber.value  || '}' ELSE '' END ||
            CASE WHEN bibpages.value   IS NOT NULL THEN E',\n  Pages   = {' || regexp_replace(bibpages.value, E'(\\d)-([\\d])', E'\\1--\\2')   || '}' ELSE '' END ||
            CASE WHEN biburl.value     IS NOT NULL THEN E',\n  URL     = {' ||
                  replace(replace(replace(replace(replace(biburl.value,
                        '_', E'\\_'),           --
                        '%', E'\\%'),           --
                        '&', E'\\&'),           --
                        '#', E'\\#'),           --
                        '~', E'\\~{}')          --
                   || '}'
                 ELSE '' END ||
            CASE WHEN bibdoi.value     IS NOT NULL THEN E',\n  DOI     = {' ||
                  replace(replace(bibdoi.value,
                        '_', E'\\_'),           --
                        '&', E'\\&')            --
                   || '}'
                 ELSE '' END ||
            CASE WHEN bibpmid.value    IS NOT NULL THEN E',\n  PMID    = {' || bibpmid.value    || '}' ELSE '' END ||
            CASE WHEN bibeprint.value  IS NOT NULL THEN E',\n  EPrint  = {' ||
                  replace(replace(replace(replace(bibeprint.value,
                        '_', E'\\_'),           --
                        '%', E'\\%'),           --
                        '&', E'\\&'),           --
                        '~', E'\\~{}')          --
                   || '}'
                 ELSE '' END ||
            CASE WHEN bibin.value      IS NOT NULL THEN E',\n  In      = {' || bibin.value      || '}' ELSE '' END ||
            CASE WHEN bibissn.value    IS NOT NULL THEN E',\n  ISSN    = {' || bibissn.value    || '}' ELSE '' END ||
            E',\n}\n'
            AS bibentry
--         p.source         AS source,
--         p.rank           AS rank,
    FROM (SELECT DISTINCT source, package, rank FROM bibref) p
    LEFT OUTER JOIN bibref bibkey     ON p.source = bibkey.source     AND bibkey.rank     = p.rank AND bibkey.package     = p.package AND bibkey.key     = 'bibtex'
    LEFT OUTER JOIN bibref bibyear    ON p.source = bibyear.source    AND bibyear.rank    = p.rank AND bibyear.package    = p.package AND bibyear.key    = 'year'  
    LEFT OUTER JOIN bibref bibmonth   ON p.source = bibmonth.source   AND bibmonth.rank   = p.rank AND bibmonth.package   = p.package AND bibmonth.key   = 'month'  
    LEFT OUTER JOIN bibref bibtitle   ON p.source = bibtitle.source   AND bibtitle.rank   = p.rank AND bibtitle.package   = p.package AND bibtitle.key   = 'title'  
    LEFT OUTER JOIN bibref bibbooktitle ON p.source = bibbooktitle.source AND bibbooktitle.rank = p.rank AND bibbooktitle.package = p.package AND bibbooktitle.key = 'booktitle'  
    LEFT OUTER JOIN bibref bibauthor  ON p.source = bibauthor.source  AND bibauthor.rank  = p.rank AND bibauthor.package  = p.package AND bibauthor.key  = 'author'
    LEFT OUTER JOIN bibref bibjournal ON p.source = bibjournal.source AND bibjournal.rank = p.rank AND bibjournal.package = p.package AND bibjournal.key = 'journal'
    LEFT OUTER JOIN bibref bibaddress ON p.source = bibaddress.source AND bibaddress.rank = p.rank AND bibaddress.package = p.package AND bibaddress.key = 'address'
    LEFT OUTER JOIN bibref bibpublisher ON p.source = bibpublisher.source AND bibpublisher.rank = p.rank AND bibpublisher.package = p.package AND bibpublisher.key = 'publisher'
    LEFT OUTER JOIN bibref bibvolume  ON p.source = bibvolume.source  AND bibvolume.rank  = p.rank AND bibvolume.package  = p.package AND bibvolume.key  = 'volume'
    LEFT OUTER JOIN bibref bibdoi     ON p.source = bibdoi.source     AND bibdoi.rank     = p.rank AND bibdoi.package     = p.package AND bibdoi.key     = 'doi'
    LEFT OUTER JOIN bibref bibpmid    ON p.source = bibpmid.source    AND bibpmid.rank    = p.rank AND bibpmid.package    = p.package AND bibpmid.key    = 'pmid'
    LEFT OUTER JOIN bibref biburl     ON p.source = biburl.source     AND biburl.rank     = p.rank AND biburl.package     = p.package AND biburl.key     = 'url'
    LEFT OUTER JOIN bibref bibnumber  ON p.source = bibnumber.source  AND bibnumber.rank  = p.rank AND bibnumber.package  = p.package AND bibnumber.key  = 'number'
    LEFT OUTER JOIN bibref bibpages   ON p.source = bibpages.source   AND bibpages.rank   = p.rank AND bibpages.package   = p.package AND bibpages.key   = 'pages'
    LEFT OUTER JOIN bibref bibeprint  ON p.source = bibeprint.source  AND bibeprint.rank  = p.rank AND bibeprint.package  = p.package AND bibeprint.key  = 'eprint'
    LEFT OUTER JOIN bibref bibin      ON p.source = bibin.source      AND bibin.rank      = p.rank AND bibin.package      = p.package AND bibin.key      = 'in'
    LEFT OUTER JOIN bibref bibissn    ON p.source = bibissn.source    AND bibissn.rank    = p.rank AND bibissn.package    = p.package AND bibissn.key    = 'issn'
    ORDER BY bibentry -- p.source
;
$$;


ALTER FUNCTION public.bibtex() OWNER TO udd;

--
-- Name: bibtex_example_data(); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.bibtex_example_data() RETURNS SETOF record
    LANGUAGE sql
    AS $$
SELECT package, source, bibkey, description FROM (
  SELECT -- DISTINCT
         p.package        AS package,
         p.source         AS source,
         b.package        AS bpackage,
         b.value          AS bibkey,
         replace(replace(replace(replace(p.description, E'\xc2\xa0', E'\\ '), E'#', E'\\#'), E'&', E'\\&'), E'_', E'\\_') AS description -- replace non-breaking spaces to TeX syntax and escape '#'
    FROM ( -- Make sure we have only one (package,source,description) record fitting the latest release with highest version
       SELECT package, source, description FROM
         (SELECT *, rank() OVER (PARTITION BY package ORDER BY rsort DESC, version DESC) FROM
           (SELECT DISTINCT package, source, description, sort as rsort, version FROM packages p
              JOIN releases r ON p.release = r. release
           ) tmp
         ) tmp WHERE rank = 1
    ) p
    JOIN (SELECT DISTINCT source, package, value FROM bibref WHERE key = 'bibtex') b ON b.source = p.source
 ) tmp
 WHERE package = bpackage OR bpackage = ''
 ORDER BY package, bibkey
;
$$;


ALTER FUNCTION public.bibtex_example_data() OWNER TO udd;

--
-- Name: blends_metapackage_translations(text[]); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.blends_metapackage_translations(text[]) RETURNS SETOF record
    LANGUAGE sql
    AS $_$
  SELECT
         p.package,
         p.description,     en.long_description_en,
         cs.description_cs, cs.long_description_cs,
         da.description_da, da.long_description_da,
         de.description_de, de.long_description_de,
         es.description_es, es.long_description_es,
         fi.description_fi, fi.long_description_fi,
         fr.description_fr, fr.long_description_fr,
         hu.description_hu, hu.long_description_hu,
         it.description_it, it.long_description_it,
         ja.description_ja, ja.long_description_ja,
         ko.description_ko, ko.long_description_ko,
         nl.description_nl, nl.long_description_nl,
         pl.description_pl, pl.long_description_pl,
         pt_BR.description_pt_BR, pt_BR.long_description_pt_BR,
         ru.description_ru, ru.long_description_ru,
         sk.description_sk, sk.long_description_sk,
         sr.description_sr, sr.long_description_sr,
         sv.description_sv, sv.long_description_sv,
         uk.description_uk, uk.long_description_uk,
         vi.description_vi, vi.long_description_vi,
         zh_CN.description_zh_CN, zh_CN.long_description_zh_CN,
         zh_TW.description_zh_TW, zh_TW.long_description_zh_TW
    FROM packages p
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('en', $1) AS (package text, description_en text, long_description_en text)) en ON en.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('cs', $1) AS (package text, description_cs text, long_description_cs text)) cs ON cs.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('da', $1) AS (package text, description_da text, long_description_da text)) da ON da.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('de', $1) AS (package text, description_de text, long_description_de text)) de ON de.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('es', $1) AS (package text, description_es text, long_description_es text)) es ON es.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('fi', $1) AS (package text, description_fi text, long_description_fi text)) fi ON fi.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('fr', $1) AS (package text, description_fr text, long_description_fr text)) fr ON fr.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('hu', $1) AS (package text, description_hu text, long_description_hu text)) hu ON hu.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('it', $1) AS (package text, description_it text, long_description_it text)) it ON it.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('ja', $1) AS (package text, description_ja text, long_description_ja text)) ja ON ja.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('ko', $1) AS (package text, description_ko text, long_description_ko text)) ko ON ko.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('nl', $1) AS (package text, description_nl text, long_description_nl text)) nl ON nl.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('pl', $1) AS (package text, description_pl text, long_description_pl text)) pl ON pl.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('pt_BR', $1) AS (package text, description_pt_BR text, long_description_pt_BR text)) pt_BR ON pt_BR.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('ru', $1) AS (package text, description_ru text, long_description_ru text)) ru ON ru.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('sk', $1) AS (package text, description_sk text, long_description_sk text)) sk ON sk.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('sr', $1) AS (package text, description_sr text, long_description_sr text)) sr ON sr.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('sv', $1) AS (package text, description_sv text, long_description_sv text)) sv ON sv.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('uk', $1) AS (package text, description_uk text, long_description_uk text)) uk ON uk.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('vi', $1) AS (package text, description_vi text, long_description_vi text)) vi ON vi.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('zh_CN', $1) AS (package text, description_zh_CN text, long_description_zh_CN text)) zh_CN ON zh_CN.package = p.package
    LEFT OUTER JOIN (SELECT * FROM ddtp_unique('zh_TW', $1) AS (package text, description_zh_TW text, long_description_zh_TW text)) zh_TW ON zh_TW.package = p.package
    WHERE p.package = ANY ($1)
 $_$;


ALTER FUNCTION public.blends_metapackage_translations(text[]) OWNER TO udd;

--
-- Name: blends_query_packages(text[], text[], text); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.blends_query_packages(text[], text[], text) RETURNS SETOF record
    LANGUAGE sql
    AS $_$
  SELECT DISTINCT
         p.package, p.distribution, p.release, p.component, p.version,
         p.maintainer,
         p.source, p.section, p.task, p.homepage,
         src.maintainer_name, src.maintainer_email,
         src.vcs_type, src.vcs_url, src.vcs_browser,
	 src.changed_by,
         enh.enhanced,
         rva.releases, versions, rva.architectures,
	 upstream_version AS unstable_upstream, debian_mangled_uversion AS unstable_parsed_version, status AS unstable_status,
         pop.vote, pop.recent, pop.insts,
         tags.debtags,
         screenshot_versions, large_image_urls, small_image_urls,
         bibyear.value    AS "year",
         bibtitle.value   AS "title",
         bibauthor.value  AS "authors",
         bibdoi.value     AS "doi",
         bibpmid.value    AS "pubmed",
         biburl.value     AS "url",
         bibjournal.value AS "journal",
         bibvolume.value  AS "volume",
         bibnumber.value  AS "number",
         bibpages.value   AS "pages",
         bibeprint.value  AS "eprint",
         en.description AS description_en, en.long_description AS long_description_en,
         cs.description AS description_cs, cs.long_description AS long_description_cs,
         da.description AS description_da, da.long_description AS long_description_da,
         de.description AS description_de, de.long_description AS long_description_de,
         es.description AS description_es, es.long_description AS long_description_es,
         fi.description AS description_fi, fi.long_description AS long_description_fi,
         fr.description AS description_fr, fr.long_description AS long_description_fr,
         hu.description AS description_hu, hu.long_description AS long_description_hu,
         it.description AS description_it, it.long_description AS long_description_it,
         ja.description AS description_ja, ja.long_description AS long_description_ja,
         ko.description AS description_ko, ko.long_description AS long_description_ko,
         nl.description AS description_nl, nl.long_description AS long_description_nl,
         pl.description AS description_pl, pl.long_description AS long_description_pl,
         pt_BR.description AS description_pt_BR, pt_BR.long_description AS long_description_pt_BR,
         ru.description AS description_ru, ru.long_description AS long_description_ru,
         sk.description AS description_sk, sk.long_description AS long_description_sk,
         sr.description AS description_sr, sr.long_description AS long_description_sr,
         sv.description AS description_sv, sv.long_description AS long_description_sv,
         uk.description AS description_uk, uk.long_description AS long_description_uk,
         vi.description AS description_vi, vi.long_description AS long_description_vi,
         zh_CN.description AS description_zh_CN, zh_CN.long_description AS long_description_zh_CN,
         zh_TW.description AS description_zh_TW, zh_TW.long_description AS long_description_zh_TW,
         re.remark,
         ubugs.upstream_bugs, urep.upstream_repository, edam.topics,
         biotools.entry AS biotools, omictools.entry AS omictools, bioconda.entry AS bioconda, scicrunch.entry AS scicrunch, rrid.entry AS rrid,
         bugs.bugs
    FROM (
      SELECT DISTINCT 
             package, distribution, release, component, strip_binary_upload(version) AS version,
             maintainer, source, section, task, homepage, description, description_md5
        FROM packages
       WHERE package = ANY ($1)
    ) p
    --                                                                                                                                                                   ---+  Ensure we get no old stuff from non-free
    --                                                                                                                                                                      v  packages with different architectures
    LEFT OUTER JOIN descriptions en ON en.language = 'en' AND en.package = p.package AND en.release = p.release  AND en.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions cs ON cs.language = 'cs' AND cs.package = p.package AND cs.release = p.release  AND cs.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions da ON da.language = 'da' AND da.package = p.package AND da.release = p.release  AND da.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions de ON de.language = 'de' AND de.package = p.package AND de.release = p.release  AND de.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions es ON es.language = 'es' AND es.package = p.package AND es.release = p.release  AND es.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions fi ON fi.language = 'fi' AND fi.package = p.package AND fi.release = p.release  AND fi.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions fr ON fr.language = 'fr' AND fr.package = p.package AND fr.release = p.release  AND fr.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions hu ON hu.language = 'hu' AND hu.package = p.package AND hu.release = p.release  AND hu.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions it ON it.language = 'it' AND it.package = p.package AND it.release = p.release  AND it.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions ja ON ja.language = 'ja' AND ja.package = p.package AND ja.release = p.release  AND ja.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions ko ON ko.language = 'ko' AND ko.package = p.package AND ko.release = p.release  AND ko.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions nl ON nl.language = 'nl' AND nl.package = p.package AND nl.release = p.release  AND nl.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions pl ON pl.language = 'pl' AND pl.package = p.package AND pl.release = p.release  AND pl.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions pt_BR ON pt_BR.language = 'pt_BR' AND pt_BR.package = p.package AND pt_BR.release = p.release AND pt_BR.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions ru ON ru.language = 'ru' AND ru.package = p.package AND ru.release = p.release  AND ru.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions sk ON sk.language = 'sk' AND sk.package = p.package AND sk.release = p.release  AND sk.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions sr ON sr.language = 'sr' AND sr.package = p.package AND sr.release = p.release  AND sr.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions sv ON sv.language = 'sv' AND sv.package = p.package AND sv.release = p.release  AND sv.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions uk ON uk.language = 'uk' AND uk.package = p.package AND uk.release = p.release  AND uk.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions vi ON vi.language = 'vi' AND vi.package = p.package AND vi.release = p.release  AND vi.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions zh_CN ON zh_CN.language = 'zh_CN' AND zh_CN.package = p.package AND zh_CN.release = p.release AND zh_CN.description_md5 = p.description_md5
    LEFT OUTER JOIN descriptions zh_TW ON zh_TW.language = 'zh_TW' AND zh_TW.package = p.package AND zh_TW.release = p.release AND zh_TW.description_md5 = p.description_md5
    -- extract one single package with highest version and release
    JOIN (
      -- select packages which have versions outside experimental
      SELECT px.package, strip_binary_upload(px.version) AS version, (SELECT release FROM releases WHERE sort = MAX(rx.sort)) AS release
        FROM (
           -- select highest version which is not in experimental - except if a package resides in experimental only
           SELECT pex.package, CASE WHEN pnoex.version IS NOT NULL THEN pnoex.version ELSE pex.version END AS version FROM
              (SELECT package, MAX(version) AS version FROM packages
                  WHERE package = ANY ($1)
                  GROUP BY package
              ) pex
              LEFT OUTER JOIN
              (SELECT package, MAX(version) AS version FROM packages
                  WHERE package = ANY ($1)
                    AND release != 'experimental'
                  GROUP BY package
              ) pnoex ON pex.package = pnoex.package
        ) px
        JOIN (
           -- select the release in which this version is available
           SELECT DISTINCT package, version, release FROM packages
            WHERE package = ANY ($1)
        ) py ON px.package = py.package AND px.version = py.version
        JOIN releases rx ON py.release = rx.release
        GROUP BY px.package, px.version
       ) pvar ON pvar.package = p.package AND pvar.version = p.version AND pvar.release = p.release
    -- obtain source_version of given package which is needed in cases where this is different form binary package version
    JOIN (
       SELECT DISTINCT package, source, strip_binary_upload(version) AS version,
                       strip_binary_upload(source_version) AS source_version, release,
                       maintainer_email
         FROM packages_summary WHERE package = ANY ($1)
    ) ps ON ps.package = p.package AND ps.release = p.release
    -- extract source and join with upload_history to find out latest uploader if different from Maintainer
    JOIN (
	SELECT DISTINCT s.source, strip_binary_upload(s.version) AS version,
               s.maintainer, s.release, s.maintainer_name, s.maintainer_email, s.vcs_type, s.vcs_url, s.vcs_browser,
               CASE WHEN uh.changed_by != s.maintainer THEN uh.changed_by ELSE NULL END AS changed_by
          FROM sources s
          LEFT OUTER JOIN upload_history uh ON s.source = uh.source AND s.version = uh.version
    ) src ON src.source = p.source AND src.source = ps.source
           AND src.release = p.release
           AND ( ( ps.version = p.version AND ps.version != ps.source_version ) OR
                 ( ps.version = p.version AND src.version = p.version) )
    -- join with sets of avialable versions in different releases
    JOIN (
      SELECT package, array_agg(release) AS releases,
             array_agg(CASE WHEN component = 'main' THEN version ELSE version || ' (' || component || ')' END) AS versions,
             array_agg(archs) AS architectures
          FROM (
     	    SELECT package, ptmp.release as release, strip_binary_upload(version) AS version, archs, component FROM
              ( SELECT package, release, version, array_to_string(array_sort(array_accum(architecture)),',') AS archs, component
                  FROM (
                    SELECT package,
                           release || CASE WHEN char_length(substring(distribution from '-.*')) > 0
                                        THEN substring(distribution from '-.*')
                                        ELSE '' END AS release,
                            -- make *-volatile a "pseudo-release"
                            strip_binary_upload(regexp_replace(version, '^[0-9]:', '')) AS version,
                            architecture,
                            component
                      FROM packages
	             WHERE package = ANY ($1)
                   ) AS prvac
		   GROUP BY package, version, release, component
              ) ptmp
	      JOIN releases ON releases.release = ptmp.release
              ORDER BY version, releases.sort
	    ) tmp GROUP BY package
         ) rva
         ON p.package = rva.package
    LEFT OUTER JOIN (
      SELECT DISTINCT
        source, upstream_version, debian_mangled_uversion, status
        FROM upstream
        WHERE release = 'sid' AND (status = 'Newer version available' OR status = 'newer package available') -- see https://lists.debian.org/debian-qa/2016/03/msg00018.html
    ) d ON p.source = d.source 
    LEFT OUTER JOIN popcon pop ON p.package = pop.package
    LEFT OUTER JOIN (
       SELECT package, array_agg(tag) AS debtags
         FROM debtags 
        WHERE tag NOT LIKE 'implemented-in::%'
	  AND tag NOT LIKE 'protocol::%'
          AND tag NOT LIKE '%::TODO'
          AND tag NOT LIKE '%not-yet-tagged%'
          GROUP BY package
    ) tags ON tags.package = p.package
    LEFT OUTER JOIN (
       SELECT package, 
              array_agg(version)  AS screenshot_versions,
              array_agg(large_image_url) AS large_image_urls,
              array_agg(small_image_url) AS small_image_urls 
         FROM screenshots 
         GROUP BY package
    ) sshots ON sshots.package = p.package
    -- check whether a package is enhanced by some other package
    LEFT OUTER JOIN (
      SELECT DISTINCT regexp_replace(package_version, E'\\s*\\(.*\\)', '') AS package, array_agg(enhanced_by) AS enhanced FROM (
        SELECT DISTINCT package AS enhanced_by, regexp_split_to_table(enhances, E',\\s*') AS package_version FROM packages
         WHERE enhances LIKE ANY( $2 )
      ) AS tmpenh GROUP BY package
    ) enh ON enh.package = p.package
    -- FIXME: To get reasonable querying of publications for specific packages and also multiple citations the table structure
    --        of the bibref table most probably needs to be changed to one entry per citation
    --        for the moment the specification of package is ignored because otherwise those citations would spoil the
    --        whole query
    --        example: if `bib*.package = ''` would be left out acedb-other would get more than 500 results !!!
    LEFT OUTER JOIN bibref bibyear    ON p.source = bibyear.source    AND bibyear.rank = 0    AND bibyear.key    = 'year'    AND bibyear.package = ''
    LEFT OUTER JOIN bibref bibtitle   ON p.source = bibtitle.source   AND bibtitle.rank = 0   AND bibtitle.key   = 'title'   AND bibtitle.package = ''
    LEFT OUTER JOIN bibref bibauthor  ON p.source = bibauthor.source  AND bibauthor.rank = 0  AND bibauthor.key  = 'author'  AND bibauthor.package = ''
    LEFT OUTER JOIN bibref bibdoi     ON p.source = bibdoi.source     AND bibdoi.rank = 0     AND bibdoi.key     = 'doi'     AND bibdoi.package = ''
    LEFT OUTER JOIN bibref bibpmid    ON p.source = bibpmid.source    AND bibpmid.rank = 0    AND bibpmid.key    = 'pmid'    AND bibpmid.package = ''
    LEFT OUTER JOIN bibref biburl     ON p.source = biburl.source     AND biburl.rank = 0     AND biburl.key     = 'url'     AND biburl.package = ''
    LEFT OUTER JOIN bibref bibjournal ON p.source = bibjournal.source AND bibjournal.rank = 0 AND bibjournal.key = 'journal' AND bibjournal.package = ''
    LEFT OUTER JOIN bibref bibvolume  ON p.source = bibvolume.source  AND bibvolume.rank = 0  AND bibvolume.key  = 'volume'  AND bibvolume.package = ''
    LEFT OUTER JOIN bibref bibnumber  ON p.source = bibnumber.source  AND bibnumber.rank = 0  AND bibnumber.key  = 'number'  AND bibnumber.package = ''
    LEFT OUTER JOIN bibref bibpages   ON p.source = bibpages.source   AND bibpages.rank = 0   AND bibpages.key   = 'pages'   AND bibpages.package = ''
    LEFT OUTER JOIN bibref bibeprint  ON p.source = bibeprint.source  AND bibeprint.rank = 0  AND bibeprint.key  = 'eprint'  AND bibeprint.package = ''
    LEFT OUTER JOIN blends_remarks re ON p.package = re.package AND re.task = $3
    LEFT OUTER JOIN (SELECT source, value AS upstream_bugs       FROM upstream_metadata WHERE key = 'Bug-Database') ubugs ON p.source = ubugs.source
    LEFT OUTER JOIN (SELECT source, value AS upstream_repository FROM upstream_metadata WHERE key = 'Repository')   urep  ON p.source = urep.source
    LEFT OUTER JOIN (SELECT source, package, topics              FROM edam) edam ON p.source = edam.source AND p.package = edam.package
    LEFT OUTER JOIN (SELECT source, entry FROM registry WHERE name  = 'bio.tools') biotools  ON p.source = biotools.source
    LEFT OUTER JOIN (SELECT source, entry FROM registry WHERE name  = 'OMICtools') omictools ON p.source = omictools.source
    LEFT OUTER JOIN (SELECT source, entry FROM registry WHERE name  = 'conda:bioconda') bioconda ON p.source = bioconda.source
    LEFT OUTER JOIN (SELECT source, entry FROM registry WHERE name  = 'SciCrunch') scicrunch ON p.source = scicrunch.source
    LEFT OUTER JOIN (SELECT source, entry FROM registry WHERE name  = 'RRID')      rrid      ON p.source = rrid.source
    LEFT OUTER JOIN (SELECT source, array_to_string(array_sort(array_accum(id)),',') AS bugs FROM bugs WHERE status != 'done' GROUP BY source) bugs ON p.source = bugs.source
    ORDER BY p.package
 $_$;


ALTER FUNCTION public.blends_query_packages(text[], text[], text) OWNER TO udd;

--
-- Name: bugs_rt_affects_dist(text); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.bugs_rt_affects_dist(text) RETURNS TABLE(id integer, package text, source text)
    LANGUAGE plpgsql
    AS $_$
DECLARE
releasecodename text := release_name($1);
BEGIN
RETURN QUERY

EXECUTE '
SELECT b.id id, b.package package, b.source source FROM bugs b
WHERE b.affects_' || $1 || '
AND 
(
		b.id NOT IN (SELECT t.id FROM bugs_tags t WHERE t.tag IN
			(
				SELECT tag FROM bts_tags WHERE tag_type=''release''
			)
		)
	OR	b.id IN (SELECT t.id FROM bugs_tags t WHERE t.tag = ''' || releasecodename || ''')
)
AND
	b.id NOT IN (select t.id FROM bugs_tags t WHERE t.tag = ''' || releasecodename || '-ignore'')
AND
(
		b.id in (select bugs_packages.id from sources, bugs_packages where sources.source = bugs_packages.source and release = ''' || releasecodename || ''' )
	OR	b.id in (select bugs_packages.id from packages_summary, bugs_packages where packages_summary.package = bugs_packages.package and release = ''' || releasecodename || ''')
);'
;
END

$_$;


ALTER FUNCTION public.bugs_rt_affects_dist(text) OWNER TO udd;

--
-- Name: ddtp_unique(text, text[]); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.ddtp_unique(text, text[]) RETURNS SETOF record
    LANGUAGE sql
    AS $_$
  SELECT DISTINCT d.package, d.description, d.long_description FROM descriptions d
    JOIN (
      SELECT dr.package, (SELECT release FROM releases WHERE sort = MAX(r.sort)) AS release FROM descriptions dr
        JOIN releases r ON dr.release = r.release
        WHERE language = $1 AND dr.package = ANY ($2)
        GROUP BY dr.package
    -- sometimes there are different translations of the same package version in different releases
    -- because translators moved on working inbetween releases but we need to select only one of these
    -- (the last one)
    ) duvr ON duvr.package = d.package AND duvr.release = d.release
    WHERE language = $1 AND d.package = ANY ($2)
 $_$;


ALTER FUNCTION public.ddtp_unique(text, text[]) OWNER TO udd;

--
-- Name: release_name(text); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.release_name(text) RETURNS text
    LANGUAGE sql
    AS $_$
SELECT release FROM releases WHERE role=$1;
$_$;


ALTER FUNCTION public.release_name(text) OWNER TO udd;

--
-- Name: strip_binary_upload(text); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.strip_binary_upload(text) RETURNS public.debversion
    LANGUAGE sql
    AS $_$
       SELECT CAST(regexp_replace(regexp_replace($1, E'\\+b[0-9]+$', ''), E'^[0-9]+:', '') AS debversion) ;
$_$;


ALTER FUNCTION public.strip_binary_upload(text) OWNER TO udd;

--
-- Name: versions_archs_component(text); Type: FUNCTION; Schema: public; Owner: udd
--

CREATE FUNCTION public.versions_archs_component(text) RETURNS SETOF record
    LANGUAGE sql
    AS $_$
       SELECT p.release, version, archs, component FROM
          ( SELECT release || CASE WHEN char_length(substring(distribution from '-.*')) > 0
                                        THEN substring(distribution from '-.*')
                                        ELSE '' END AS release,
                            -- make *-volatile a "pseudo-release"
                        regexp_replace(version, '^[0-9]:', '') AS version,
                        array_to_string(array_sort(array_accum(architecture)),',') AS archs,
                        component
                    FROM packages
	           WHERE package = $1
		   GROUP BY version, release, distribution, component
          ) p
	  JOIN releases ON releases.release = p.release
	  ORDER BY releases.sort, version;
 $_$;


ALTER FUNCTION public.versions_archs_component(text) OWNER TO udd;

--
-- Name: array_accum(anyelement); Type: AGGREGATE; Schema: public; Owner: udd
--

CREATE AGGREGATE public.array_accum(anyelement) (
    SFUNC = array_append,
    STYPE = anyarray,
    INITCOND = '{}'
);


ALTER AGGREGATE public.array_accum(anyelement) OWNER TO udd;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: sources_count; Type: TABLE; Schema: history; Owner: udd
--

CREATE TABLE history.sources_count (
    ts timestamp without time zone NOT NULL,
    total_sid_main integer,
    total_sid_contrib integer,
    total_sid_nonfree integer,
    vcstype_arch integer,
    vcstype_bzr integer,
    vcstype_cvs integer,
    vcstype_darcs integer,
    vcstype_git integer,
    vcstype_hg integer,
    vcstype_mtn integer,
    vcstype_svn integer,
    format_3native integer,
    format_3quilt integer
);


ALTER TABLE history.sources_count OWNER TO udd;

--
-- Name: sources; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.sources (
    source text NOT NULL,
    version public.debversion NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    format text,
    files text,
    uploaders text,
    bin text,
    architecture text,
    standards_version text,
    homepage text,
    build_depends text,
    build_depends_indep text,
    build_conflicts text,
    build_conflicts_indep text,
    priority text,
    section text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    vcs_type text,
    vcs_url text,
    vcs_browser text,
    python_version text,
    ruby_versions text,
    checksums_sha1 text,
    checksums_sha256 text,
    original_maintainer text,
    dm_upload_allowed boolean,
    testsuite text,
    autobuild text,
    extra_source_only boolean,
    build_depends_arch text,
    build_conflicts_arch text
);


ALTER TABLE public.sources OWNER TO udd;

--
-- Name: uploaders; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.uploaders (
    source text,
    version public.debversion,
    distribution text,
    release text,
    component text,
    uploader text,
    name text,
    email text
);


ALTER TABLE public.uploaders OWNER TO udd;

--
-- Name: carnivore_keys; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.carnivore_keys (
    id integer,
    key text NOT NULL,
    key_type text NOT NULL
);


ALTER TABLE public.carnivore_keys OWNER TO udd;

--
-- Name: carnivore_login; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.carnivore_login (
    id integer NOT NULL,
    login text
);


ALTER TABLE public.carnivore_login OWNER TO udd;

--
-- Name: active_dds; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.active_dds AS
 SELECT DISTINCT carnivore_login.id,
    carnivore_login.login
   FROM public.carnivore_login,
    public.carnivore_keys
  WHERE ((carnivore_keys.id = carnivore_login.id) AND (carnivore_keys.key_type = 'keyring'::text));


ALTER TABLE public.active_dds OWNER TO udd;

--
-- Name: archived_bugs; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_bugs (
    id integer NOT NULL,
    package text,
    source text,
    arrival timestamp without time zone,
    status text,
    severity public.bugs_severity,
    submitter text,
    submitter_name text,
    submitter_email text,
    owner text,
    owner_name text,
    owner_email text,
    done text,
    done_name text,
    done_email text,
    title text,
    last_modified timestamp without time zone,
    forwarded text,
    affects_oldstable boolean,
    affects_stable boolean,
    affects_testing boolean,
    affects_unstable boolean,
    affects_experimental boolean,
    affected_packages text,
    affected_sources text
);


ALTER TABLE public.archived_bugs OWNER TO udd;

--
-- Name: bugs; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs (
    id integer NOT NULL,
    package text,
    source text,
    arrival timestamp without time zone,
    status text,
    severity public.bugs_severity,
    submitter text,
    submitter_name text,
    submitter_email text,
    owner text,
    owner_name text,
    owner_email text,
    done text,
    done_name text,
    done_email text,
    title text,
    last_modified timestamp without time zone,
    forwarded text,
    affects_oldstable boolean,
    affects_stable boolean,
    affects_testing boolean,
    affects_unstable boolean,
    affects_experimental boolean,
    affected_packages text,
    affected_sources text
);


ALTER TABLE public.bugs OWNER TO udd;

--
-- Name: all_bugs; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.all_bugs AS
 SELECT bugs.id,
    bugs.package,
    bugs.source,
    bugs.arrival,
    bugs.status,
    bugs.severity,
    bugs.submitter,
    bugs.submitter_name,
    bugs.submitter_email,
    bugs.owner,
    bugs.owner_name,
    bugs.owner_email,
    bugs.done,
    bugs.done_name,
    bugs.done_email,
    bugs.title,
    bugs.last_modified,
    bugs.forwarded,
    bugs.affects_oldstable,
    bugs.affects_stable,
    bugs.affects_testing,
    bugs.affects_unstable,
    bugs.affects_experimental,
    bugs.affected_packages,
    bugs.affected_sources
   FROM public.bugs
UNION ALL
 SELECT archived_bugs.id,
    archived_bugs.package,
    archived_bugs.source,
    archived_bugs.arrival,
    archived_bugs.status,
    archived_bugs.severity,
    archived_bugs.submitter,
    archived_bugs.submitter_name,
    archived_bugs.submitter_email,
    archived_bugs.owner,
    archived_bugs.owner_name,
    archived_bugs.owner_email,
    archived_bugs.done,
    archived_bugs.done_name,
    archived_bugs.done_email,
    archived_bugs.title,
    archived_bugs.last_modified,
    archived_bugs.forwarded,
    archived_bugs.affects_oldstable,
    archived_bugs.affects_stable,
    archived_bugs.affects_testing,
    archived_bugs.affects_unstable,
    archived_bugs.affects_experimental,
    archived_bugs.affected_packages,
    archived_bugs.affected_sources
   FROM public.archived_bugs;


ALTER TABLE public.all_bugs OWNER TO udd;

--
-- Name: derivatives_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.derivatives_packages (
    package text NOT NULL,
    version public.debversion NOT NULL,
    architecture text NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    description text,
    description_md5 text,
    source text,
    source_version public.debversion,
    essential text,
    depends text,
    recommends text,
    suggests text,
    enhances text,
    pre_depends text,
    breaks text,
    installed_size bigint,
    homepage text,
    size bigint,
    build_essential text,
    origin text,
    sha1 text,
    replaces text,
    section text,
    md5sum text,
    bugs text,
    priority text,
    tag text,
    task text,
    python_version text,
    ruby_versions text,
    provides text,
    conflicts text,
    sha256 text,
    original_maintainer text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    multi_arch text,
    package_type text
);


ALTER TABLE public.derivatives_packages OWNER TO udd;

--
-- Name: packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.packages (
    package text NOT NULL,
    version public.debversion NOT NULL,
    architecture text NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    description text,
    description_md5 text,
    source text,
    source_version public.debversion,
    essential text,
    depends text,
    recommends text,
    suggests text,
    enhances text,
    pre_depends text,
    breaks text,
    installed_size bigint,
    homepage text,
    size bigint,
    build_essential text,
    origin text,
    sha1 text,
    replaces text,
    section text,
    md5sum text,
    bugs text,
    priority text,
    tag text,
    task text,
    python_version text,
    ruby_versions text,
    provides text,
    conflicts text,
    sha256 text,
    original_maintainer text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    multi_arch text,
    package_type text
);


ALTER TABLE public.packages OWNER TO udd;

--
-- Name: ubuntu_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_packages (
    package text NOT NULL,
    version public.debversion NOT NULL,
    architecture text NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    description text,
    description_md5 text,
    source text,
    source_version public.debversion,
    essential text,
    depends text,
    recommends text,
    suggests text,
    enhances text,
    pre_depends text,
    breaks text,
    installed_size bigint,
    homepage text,
    size bigint,
    build_essential text,
    origin text,
    sha1 text,
    replaces text,
    section text,
    md5sum text,
    bugs text,
    priority text,
    tag text,
    task text,
    python_version text,
    ruby_versions text,
    provides text,
    conflicts text,
    sha256 text,
    original_maintainer text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    multi_arch text,
    package_type text
);


ALTER TABLE public.ubuntu_packages OWNER TO udd;

--
-- Name: all_packages; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.all_packages AS
 SELECT packages.package,
    packages.version,
    packages.architecture,
    packages.maintainer,
    packages.maintainer_name,
    packages.maintainer_email,
    packages.description,
    packages.description_md5,
    packages.source,
    packages.source_version,
    packages.essential,
    packages.depends,
    packages.recommends,
    packages.suggests,
    packages.enhances,
    packages.pre_depends,
    packages.breaks,
    packages.installed_size,
    packages.homepage,
    packages.size,
    packages.build_essential,
    packages.origin,
    packages.sha1,
    packages.replaces,
    packages.section,
    packages.md5sum,
    packages.bugs,
    packages.priority,
    packages.tag,
    packages.task,
    packages.python_version,
    packages.ruby_versions,
    packages.provides,
    packages.conflicts,
    packages.sha256,
    packages.original_maintainer,
    packages.distribution,
    packages.release,
    packages.component,
    packages.multi_arch,
    packages.package_type
   FROM public.packages
UNION ALL
 SELECT ubuntu_packages.package,
    ubuntu_packages.version,
    ubuntu_packages.architecture,
    ubuntu_packages.maintainer,
    ubuntu_packages.maintainer_name,
    ubuntu_packages.maintainer_email,
    ubuntu_packages.description,
    ubuntu_packages.description_md5,
    ubuntu_packages.source,
    ubuntu_packages.source_version,
    ubuntu_packages.essential,
    ubuntu_packages.depends,
    ubuntu_packages.recommends,
    ubuntu_packages.suggests,
    ubuntu_packages.enhances,
    ubuntu_packages.pre_depends,
    ubuntu_packages.breaks,
    ubuntu_packages.installed_size,
    ubuntu_packages.homepage,
    ubuntu_packages.size,
    ubuntu_packages.build_essential,
    ubuntu_packages.origin,
    ubuntu_packages.sha1,
    ubuntu_packages.replaces,
    ubuntu_packages.section,
    ubuntu_packages.md5sum,
    ubuntu_packages.bugs,
    ubuntu_packages.priority,
    ubuntu_packages.tag,
    ubuntu_packages.task,
    ubuntu_packages.python_version,
    ubuntu_packages.ruby_versions,
    ubuntu_packages.provides,
    ubuntu_packages.conflicts,
    ubuntu_packages.sha256,
    ubuntu_packages.original_maintainer,
    ubuntu_packages.distribution,
    ubuntu_packages.release,
    ubuntu_packages.component,
    ubuntu_packages.multi_arch,
    ubuntu_packages.package_type
   FROM public.ubuntu_packages
UNION ALL
 SELECT derivatives_packages.package,
    derivatives_packages.version,
    derivatives_packages.architecture,
    derivatives_packages.maintainer,
    derivatives_packages.maintainer_name,
    derivatives_packages.maintainer_email,
    derivatives_packages.description,
    derivatives_packages.description_md5,
    derivatives_packages.source,
    derivatives_packages.source_version,
    derivatives_packages.essential,
    derivatives_packages.depends,
    derivatives_packages.recommends,
    derivatives_packages.suggests,
    derivatives_packages.enhances,
    derivatives_packages.pre_depends,
    derivatives_packages.breaks,
    derivatives_packages.installed_size,
    derivatives_packages.homepage,
    derivatives_packages.size,
    derivatives_packages.build_essential,
    derivatives_packages.origin,
    derivatives_packages.sha1,
    derivatives_packages.replaces,
    derivatives_packages.section,
    derivatives_packages.md5sum,
    derivatives_packages.bugs,
    derivatives_packages.priority,
    derivatives_packages.tag,
    derivatives_packages.task,
    derivatives_packages.python_version,
    derivatives_packages.ruby_versions,
    derivatives_packages.provides,
    derivatives_packages.conflicts,
    derivatives_packages.sha256,
    derivatives_packages.original_maintainer,
    derivatives_packages.distribution,
    derivatives_packages.release,
    derivatives_packages.component,
    derivatives_packages.multi_arch,
    derivatives_packages.package_type
   FROM public.derivatives_packages;


ALTER TABLE public.all_packages OWNER TO udd;

--
-- Name: derivatives_packages_distrelcomparch; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.derivatives_packages_distrelcomparch (
    distribution text,
    release text,
    component text,
    architecture text
);


ALTER TABLE public.derivatives_packages_distrelcomparch OWNER TO udd;

--
-- Name: packages_distrelcomparch; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.packages_distrelcomparch (
    distribution text,
    release text,
    component text,
    architecture text
);


ALTER TABLE public.packages_distrelcomparch OWNER TO udd;

--
-- Name: ubuntu_packages_distrelcomparch; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_packages_distrelcomparch (
    distribution text,
    release text,
    component text,
    architecture text
);


ALTER TABLE public.ubuntu_packages_distrelcomparch OWNER TO udd;

--
-- Name: all_packages_distrelcomparch; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.all_packages_distrelcomparch AS
 SELECT packages_distrelcomparch.distribution,
    packages_distrelcomparch.release,
    packages_distrelcomparch.component,
    packages_distrelcomparch.architecture
   FROM public.packages_distrelcomparch
UNION ALL
 SELECT ubuntu_packages_distrelcomparch.distribution,
    ubuntu_packages_distrelcomparch.release,
    ubuntu_packages_distrelcomparch.component,
    ubuntu_packages_distrelcomparch.architecture
   FROM public.ubuntu_packages_distrelcomparch
UNION ALL
 SELECT derivatives_packages_distrelcomparch.distribution,
    derivatives_packages_distrelcomparch.release,
    derivatives_packages_distrelcomparch.component,
    derivatives_packages_distrelcomparch.architecture
   FROM public.derivatives_packages_distrelcomparch;


ALTER TABLE public.all_packages_distrelcomparch OWNER TO udd;

--
-- Name: derivatives_sources; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.derivatives_sources (
    source text NOT NULL,
    version public.debversion NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    format text,
    files text,
    uploaders text,
    bin text,
    architecture text,
    standards_version text,
    homepage text,
    build_depends text,
    build_depends_indep text,
    build_conflicts text,
    build_conflicts_indep text,
    priority text,
    section text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    vcs_type text,
    vcs_url text,
    vcs_browser text,
    python_version text,
    ruby_versions text,
    checksums_sha1 text,
    checksums_sha256 text,
    original_maintainer text,
    dm_upload_allowed boolean,
    testsuite text,
    autobuild text,
    extra_source_only boolean,
    build_depends_arch text,
    build_conflicts_arch text
);


ALTER TABLE public.derivatives_sources OWNER TO udd;

--
-- Name: ubuntu_sources; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_sources (
    source text NOT NULL,
    version public.debversion NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    format text,
    files text,
    uploaders text,
    bin text,
    architecture text,
    standards_version text,
    homepage text,
    build_depends text,
    build_depends_indep text,
    build_conflicts text,
    build_conflicts_indep text,
    priority text,
    section text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    vcs_type text,
    vcs_url text,
    vcs_browser text,
    python_version text,
    ruby_versions text,
    checksums_sha1 text,
    checksums_sha256 text,
    original_maintainer text,
    dm_upload_allowed boolean,
    testsuite text,
    autobuild text,
    extra_source_only boolean,
    build_depends_arch text,
    build_conflicts_arch text
);


ALTER TABLE public.ubuntu_sources OWNER TO udd;

--
-- Name: all_sources; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.all_sources AS
 SELECT sources.source,
    sources.version,
    sources.maintainer,
    sources.maintainer_name,
    sources.maintainer_email,
    sources.format,
    sources.files,
    sources.uploaders,
    sources.bin,
    sources.architecture,
    sources.standards_version,
    sources.homepage,
    sources.build_depends,
    sources.build_depends_indep,
    sources.build_conflicts,
    sources.build_conflicts_indep,
    sources.priority,
    sources.section,
    sources.distribution,
    sources.release,
    sources.component,
    sources.vcs_type,
    sources.vcs_url,
    sources.vcs_browser,
    sources.python_version,
    sources.ruby_versions,
    sources.checksums_sha1,
    sources.checksums_sha256,
    sources.original_maintainer,
    sources.dm_upload_allowed,
    sources.testsuite,
    sources.autobuild,
    sources.extra_source_only
   FROM public.sources
UNION ALL
 SELECT ubuntu_sources.source,
    ubuntu_sources.version,
    ubuntu_sources.maintainer,
    ubuntu_sources.maintainer_name,
    ubuntu_sources.maintainer_email,
    ubuntu_sources.format,
    ubuntu_sources.files,
    ubuntu_sources.uploaders,
    ubuntu_sources.bin,
    ubuntu_sources.architecture,
    ubuntu_sources.standards_version,
    ubuntu_sources.homepage,
    ubuntu_sources.build_depends,
    ubuntu_sources.build_depends_indep,
    ubuntu_sources.build_conflicts,
    ubuntu_sources.build_conflicts_indep,
    ubuntu_sources.priority,
    ubuntu_sources.section,
    ubuntu_sources.distribution,
    ubuntu_sources.release,
    ubuntu_sources.component,
    ubuntu_sources.vcs_type,
    ubuntu_sources.vcs_url,
    ubuntu_sources.vcs_browser,
    ubuntu_sources.python_version,
    ubuntu_sources.ruby_versions,
    ubuntu_sources.checksums_sha1,
    ubuntu_sources.checksums_sha256,
    ubuntu_sources.original_maintainer,
    ubuntu_sources.dm_upload_allowed,
    ubuntu_sources.testsuite,
    ubuntu_sources.autobuild,
    ubuntu_sources.extra_source_only
   FROM public.ubuntu_sources
UNION ALL
 SELECT derivatives_sources.source,
    derivatives_sources.version,
    derivatives_sources.maintainer,
    derivatives_sources.maintainer_name,
    derivatives_sources.maintainer_email,
    derivatives_sources.format,
    derivatives_sources.files,
    derivatives_sources.uploaders,
    derivatives_sources.bin,
    derivatives_sources.architecture,
    derivatives_sources.standards_version,
    derivatives_sources.homepage,
    derivatives_sources.build_depends,
    derivatives_sources.build_depends_indep,
    derivatives_sources.build_conflicts,
    derivatives_sources.build_conflicts_indep,
    derivatives_sources.priority,
    derivatives_sources.section,
    derivatives_sources.distribution,
    derivatives_sources.release,
    derivatives_sources.component,
    derivatives_sources.vcs_type,
    derivatives_sources.vcs_url,
    derivatives_sources.vcs_browser,
    derivatives_sources.python_version,
    derivatives_sources.ruby_versions,
    derivatives_sources.checksums_sha1,
    derivatives_sources.checksums_sha256,
    derivatives_sources.original_maintainer,
    derivatives_sources.dm_upload_allowed,
    derivatives_sources.testsuite,
    derivatives_sources.autobuild,
    derivatives_sources.extra_source_only
   FROM public.derivatives_sources;


ALTER TABLE public.all_sources OWNER TO udd;

--
-- Name: archived_bugs_blockedby; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_bugs_blockedby (
    id integer NOT NULL,
    blocker integer NOT NULL
);


ALTER TABLE public.archived_bugs_blockedby OWNER TO udd;

--
-- Name: archived_bugs_blocks; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_bugs_blocks (
    id integer NOT NULL,
    blocked integer NOT NULL
);


ALTER TABLE public.archived_bugs_blocks OWNER TO udd;

--
-- Name: archived_bugs_fixed_in; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_bugs_fixed_in (
    id integer NOT NULL,
    version text NOT NULL
);


ALTER TABLE public.archived_bugs_fixed_in OWNER TO udd;

--
-- Name: archived_bugs_found_in; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_bugs_found_in (
    id integer NOT NULL,
    version text NOT NULL
);


ALTER TABLE public.archived_bugs_found_in OWNER TO udd;

--
-- Name: archived_bugs_merged_with; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_bugs_merged_with (
    id integer NOT NULL,
    merged_with integer NOT NULL
);


ALTER TABLE public.archived_bugs_merged_with OWNER TO udd;

--
-- Name: archived_bugs_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_bugs_packages (
    id integer NOT NULL,
    package text NOT NULL,
    source text
);


ALTER TABLE public.archived_bugs_packages OWNER TO udd;

--
-- Name: archived_bugs_stamps; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_bugs_stamps (
    id integer NOT NULL,
    update_requested bigint,
    db_updated bigint
);


ALTER TABLE public.archived_bugs_stamps OWNER TO udd;

--
-- Name: archived_bugs_tags; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_bugs_tags (
    id integer NOT NULL,
    tag text NOT NULL
);


ALTER TABLE public.archived_bugs_tags OWNER TO udd;

--
-- Name: archived_descriptions; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_descriptions (
    package text NOT NULL,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    language text NOT NULL,
    description text NOT NULL,
    long_description text NOT NULL,
    description_md5 text NOT NULL
);


ALTER TABLE public.archived_descriptions OWNER TO udd;

--
-- Name: archived_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_packages (
    package text NOT NULL,
    version public.debversion NOT NULL,
    architecture text NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    description text,
    description_md5 text,
    source text,
    source_version public.debversion,
    essential text,
    depends text,
    recommends text,
    suggests text,
    enhances text,
    pre_depends text,
    breaks text,
    installed_size bigint,
    homepage text,
    size bigint,
    build_essential text,
    origin text,
    sha1 text,
    replaces text,
    section text,
    md5sum text,
    bugs text,
    priority text,
    tag text,
    task text,
    python_version text,
    ruby_versions text,
    provides text,
    conflicts text,
    sha256 text,
    original_maintainer text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    multi_arch text
);


ALTER TABLE public.archived_packages OWNER TO udd;

--
-- Name: archived_packages_distrelcomparch; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_packages_distrelcomparch (
    distribution text,
    release text,
    component text,
    architecture text
);


ALTER TABLE public.archived_packages_distrelcomparch OWNER TO udd;

--
-- Name: archived_packages_summary; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_packages_summary (
    package text NOT NULL,
    version public.debversion NOT NULL,
    source text,
    source_version public.debversion,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL
);


ALTER TABLE public.archived_packages_summary OWNER TO udd;

--
-- Name: archived_sources; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_sources (
    source text NOT NULL,
    version public.debversion NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    format text,
    files text,
    uploaders text,
    bin text,
    architecture text,
    standards_version text,
    homepage text,
    build_depends text,
    build_depends_indep text,
    build_conflicts text,
    build_conflicts_indep text,
    priority text,
    section text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    vcs_type text,
    vcs_url text,
    vcs_browser text,
    python_version text,
    ruby_versions text,
    checksums_sha1 text,
    checksums_sha256 text,
    original_maintainer text,
    dm_upload_allowed boolean,
    testsuite text,
    autobuild text,
    extra_source_only boolean,
    build_depends_arch text,
    build_conflicts_arch text
);


ALTER TABLE public.archived_sources OWNER TO udd;

--
-- Name: archived_uploaders; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.archived_uploaders (
    source text,
    version public.debversion,
    distribution text,
    release text,
    component text,
    uploader text,
    name text,
    email text
);


ALTER TABLE public.archived_uploaders OWNER TO udd;

--
-- Name: bugs_merged_with; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs_merged_with (
    id integer NOT NULL,
    merged_with integer NOT NULL
);


ALTER TABLE public.bugs_merged_with OWNER TO udd;

--
-- Name: bugs_count; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.bugs_count AS
 SELECT COALESCE(b1.source, b2.source) AS source,
    COALESCE(b1.rc_bugs, (0)::bigint) AS rc_bugs,
    COALESCE(b2.all_bugs, (0)::bigint) AS all_bugs
   FROM (( SELECT bugs.source,
            count(*) AS rc_bugs
           FROM public.bugs
          WHERE ((bugs.severity >= 'serious'::public.bugs_severity) AND (bugs.status = 'pending'::text) AND (NOT (bugs.id IN ( SELECT bugs_merged_with.id
                   FROM public.bugs_merged_with
                  WHERE (bugs_merged_with.id > bugs_merged_with.merged_with)))))
          GROUP BY bugs.source) b1
     FULL JOIN ( SELECT bugs.source,
            count(*) AS all_bugs
           FROM public.bugs
          WHERE ((bugs.status = 'pending'::text) AND (NOT (bugs.id IN ( SELECT bugs_merged_with.id
                   FROM public.bugs_merged_with
                  WHERE (bugs_merged_with.id > bugs_merged_with.merged_with)))))
          GROUP BY bugs.source) b2 ON ((b1.source = b2.source)));


ALTER TABLE public.bugs_count OWNER TO udd;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.migrations (
    source text NOT NULL,
    in_testing date,
    testing_version public.debversion,
    in_unstable date,
    unstable_version public.debversion,
    sync date,
    sync_version public.debversion,
    first_seen date
);


ALTER TABLE public.migrations OWNER TO udd;

--
-- Name: orphaned_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.orphaned_packages (
    source text NOT NULL,
    type text,
    bug integer,
    description text,
    orphaned_time timestamp without time zone
);


ALTER TABLE public.orphaned_packages OWNER TO udd;

--
-- Name: popcon_src; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.popcon_src (
    source text NOT NULL,
    insts integer,
    vote integer,
    olde integer,
    recent integer,
    nofiles integer
);


ALTER TABLE public.popcon_src OWNER TO udd;

--
-- Name: sources_uniq; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.sources_uniq AS
 SELECT s1.source,
    s1.version,
    s1.maintainer,
    s1.maintainer_name,
    s1.maintainer_email,
    s1.format,
    s1.files,
    s1.uploaders,
    s1.bin,
    s1.architecture,
    s1.standards_version,
    s1.homepage,
    s1.build_depends,
    s1.build_depends_indep,
    s1.build_conflicts,
    s1.build_conflicts_indep,
    s1.priority,
    s1.section,
    s1.distribution,
    s1.release,
    s1.component,
    s1.vcs_type,
    s1.vcs_url,
    s1.vcs_browser,
    s1.python_version,
    s1.ruby_versions,
    s1.checksums_sha1,
    s1.checksums_sha256,
    s1.original_maintainer,
    s1.dm_upload_allowed,
    s1.testsuite,
    s1.autobuild,
    s1.extra_source_only
   FROM public.sources s1
  WHERE (NOT (EXISTS ( SELECT s2.source,
            s2.version,
            s2.maintainer,
            s2.maintainer_name,
            s2.maintainer_email,
            s2.format,
            s2.files,
            s2.uploaders,
            s2.bin,
            s2.architecture,
            s2.standards_version,
            s2.homepage,
            s2.build_depends,
            s2.build_depends_indep,
            s2.build_conflicts,
            s2.build_conflicts_indep,
            s2.priority,
            s2.section,
            s2.distribution,
            s2.release,
            s2.component,
            s2.vcs_type,
            s2.vcs_url,
            s2.vcs_browser,
            s2.python_version,
            s2.ruby_versions,
            s2.checksums_sha1,
            s2.checksums_sha256,
            s2.original_maintainer,
            s2.dm_upload_allowed,
            s2.testsuite,
            s2.autobuild,
            s2.extra_source_only
           FROM public.sources s2
          WHERE ((s1.source = s2.source) AND (s1.distribution = s2.distribution) AND (s1.release = s2.release) AND (s1.component = s2.component) AND (s2.version OPERATOR(public.>) s1.version)))));


ALTER TABLE public.sources_uniq OWNER TO udd;

--
-- Name: upload_history; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.upload_history (
    source text NOT NULL,
    version public.debversion NOT NULL,
    date timestamp with time zone,
    changed_by text,
    changed_by_name text,
    changed_by_email text,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    nmu boolean,
    signed_by text,
    signed_by_name text,
    signed_by_email text,
    key_id text,
    distribution text,
    file text,
    fingerprint text
);


ALTER TABLE public.upload_history OWNER TO udd;

--
-- Name: upload_history_nmus; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.upload_history_nmus AS
 SELECT uh1.source,
    count(*) AS nmus
   FROM public.upload_history uh1,
    ( SELECT upload_history.source,
            max(upload_history.date) AS date
           FROM public.upload_history
          WHERE (upload_history.nmu = false)
          GROUP BY upload_history.source) uh2
  WHERE ((uh1.nmu = true) AND (uh1.source = uh2.source) AND (uh1.date > uh2.date))
  GROUP BY uh1.source;


ALTER TABLE public.upload_history_nmus OWNER TO udd;

--
-- Name: bapase; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.bapase AS
 SELECT s.source,
    s.version,
    op.type,
    op.bug,
    op.description,
    op.orphaned_time,
    (('now'::text)::date - (op.orphaned_time)::date) AS orphaned_age,
    tm.in_testing,
    (('now'::text)::date - tm.in_testing) AS testing_age,
    tm.testing_version,
    tm.in_unstable,
    (('now'::text)::date - tm.in_unstable) AS unstable_age,
    tm.unstable_version,
    tm.sync,
    (('now'::text)::date - tm.sync) AS sync_age,
    tm.sync_version,
    tm.first_seen,
    (('now'::text)::date - tm.first_seen) AS first_seen_age,
    uh.date AS upload_date,
    (('now'::text)::date - (uh.date)::date) AS upload_age,
    uh.nmu,
    COALESCE(uhn.nmus, (0)::bigint) AS nmus,
    COALESCE(b.rc_bugs, (0)::bigint) AS rc_bugs,
    COALESCE(b.all_bugs, (0)::bigint) AS all_bugs,
    COALESCE(ps.insts, 0) AS insts,
    COALESCE(ps.vote, 0) AS vote,
    s.maintainer,
    bugs.last_modified,
    (('now'::text)::date - (bugs.last_modified)::date) AS last_modified_age
   FROM (((((((public.sources_uniq s
     LEFT JOIN public.orphaned_packages op ON ((s.source = op.source)))
     LEFT JOIN public.migrations tm ON ((s.source = tm.source)))
     LEFT JOIN public.upload_history uh ON (((s.source = uh.source) AND (s.version OPERATOR(public.=) uh.version))))
     LEFT JOIN public.upload_history_nmus uhn ON ((s.source = uhn.source)))
     LEFT JOIN public.bugs_count b ON ((s.source = b.source)))
     LEFT JOIN public.popcon_src ps ON ((s.source = ps.source)))
     LEFT JOIN public.bugs ON ((op.bug = bugs.id)))
  WHERE ((s.distribution = 'debian'::text) AND (s.release = 'sid'::text));


ALTER TABLE public.bapase OWNER TO udd;

--
-- Name: bibref; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bibref (
    source text NOT NULL,
    key text NOT NULL,
    value text NOT NULL,
    package text DEFAULT ''::text NOT NULL,
    rank integer NOT NULL
);


ALTER TABLE public.bibref OWNER TO udd;

--
-- Name: blends_dependencies; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.blends_dependencies (
    blend text NOT NULL,
    task text NOT NULL,
    package text NOT NULL,
    dependency character(1),
    distribution text,
    component text,
    provides boolean,
    CONSTRAINT blends_dependencies_component_check CHECK ((component = ANY (ARRAY['main'::text, 'main/debian-installer'::text, 'contrib'::text, 'non-free'::text, 'universe'::text, 'universe/debian-installer'::text, 'multiverse'::text, 'restricted'::text, 'local'::text]))),
    CONSTRAINT blends_dependencies_dependency_check CHECK ((dependency = ANY (ARRAY['d'::bpchar, 'i'::bpchar, 'r'::bpchar, 's'::bpchar, 'a'::bpchar]))),
    CONSTRAINT blends_dependencies_distribution_check CHECK ((distribution = ANY (ARRAY['debian'::text, 'new'::text, 'prospective'::text, 'ubuntu'::text, 'other'::text])))
);


ALTER TABLE public.blends_dependencies OWNER TO udd;

--
-- Name: blends_dependencies_alternatives; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.blends_dependencies_alternatives (
    blend text NOT NULL,
    task text NOT NULL,
    alternatives text NOT NULL,
    dependency character(1),
    distribution text,
    component text,
    contains_provides boolean,
    CONSTRAINT blends_dependencies_alternatives_component_check CHECK ((component = ANY (ARRAY['main'::text, 'main/debian-installer'::text, 'contrib'::text, 'non-free'::text, 'universe'::text, 'universe/debian-installer'::text, 'multiverse'::text, 'restricted'::text, 'local'::text]))),
    CONSTRAINT blends_dependencies_alternatives_dependency_check CHECK ((dependency = ANY (ARRAY['d'::bpchar, 'i'::bpchar, 'r'::bpchar, 's'::bpchar, 'a'::bpchar]))),
    CONSTRAINT blends_dependencies_alternatives_distribution_check CHECK ((distribution = ANY (ARRAY['debian'::text, 'new'::text, 'prospective'::text, 'ubuntu'::text, 'other'::text])))
);


ALTER TABLE public.blends_dependencies_alternatives OWNER TO udd;

--
-- Name: blends_dependencies_priorities; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.blends_dependencies_priorities (
    dependency character(1),
    priority integer,
    CONSTRAINT blends_dependencies_priorities_dependency_check CHECK ((dependency = ANY (ARRAY['d'::bpchar, 'i'::bpchar, 'r'::bpchar, 's'::bpchar, 'a'::bpchar])))
);


ALTER TABLE public.blends_dependencies_priorities OWNER TO udd;

--
-- Name: blends_metadata; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.blends_metadata (
    blend text NOT NULL,
    blendname text,
    projecturl text,
    tasksprefix text,
    homepage text,
    aliothurl text,
    projectlist text,
    logourl text,
    outputdir text,
    datadir text,
    vcsdir text,
    css text,
    advertising text,
    pkglist text,
    dehsmail text,
    distribution text
);


ALTER TABLE public.blends_metadata OWNER TO udd;

--
-- Name: blends_prospectivepackages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.blends_prospectivepackages (
    blend text,
    package text NOT NULL,
    source text,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    changed_by text,
    changed_by_name text,
    changed_by_email text,
    uploaders text,
    description text,
    long_description text,
    description_md5 text,
    homepage text,
    component text,
    section text,
    priority text,
    vcs_type text,
    vcs_url text,
    vcs_browser text,
    wnpp integer,
    wnpp_type text,
    wnpp_desc text,
    license text,
    chlog_date text,
    chlog_version public.debversion,
    CONSTRAINT check_component CHECK ((component = ANY (ARRAY['main'::text, 'contrib'::text, 'non-free'::text])))
);


ALTER TABLE public.blends_prospectivepackages OWNER TO udd;

--
-- Name: blends_remarks; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.blends_remarks (
    blend text NOT NULL,
    task text NOT NULL,
    package text NOT NULL,
    remark text
);


ALTER TABLE public.blends_remarks OWNER TO udd;

--
-- Name: blends_tasks; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.blends_tasks (
    blend text NOT NULL,
    task text NOT NULL,
    title text,
    metapackage boolean,
    metapackage_name text,
    section text,
    enhances text,
    leaf text,
    test_always_lang text,
    description text,
    long_description text,
    hashkey text
);


ALTER TABLE public.blends_tasks OWNER TO udd;

--
-- Name: blends_unknown_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.blends_unknown_packages (
    package text NOT NULL,
    pkg_url text,
    wnpp integer,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    description text,
    long_description text,
    homepage text,
    license text,
    language text,
    vcs_type text,
    vcs_url text,
    vcs_browser text,
    blend text,
    task text
);


ALTER TABLE public.blends_unknown_packages OWNER TO udd;

--
-- Name: bts_tags; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bts_tags (
    tag text,
    tag_type text
);


ALTER TABLE public.bts_tags OWNER TO udd;

--
-- Name: bugs_blockedby; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs_blockedby (
    id integer NOT NULL,
    blocker integer NOT NULL
);


ALTER TABLE public.bugs_blockedby OWNER TO udd;

--
-- Name: bugs_blocks; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs_blocks (
    id integer NOT NULL,
    blocked integer NOT NULL
);


ALTER TABLE public.bugs_blocks OWNER TO udd;

--
-- Name: bugs_fixed_in; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs_fixed_in (
    id integer NOT NULL,
    version text NOT NULL
);


ALTER TABLE public.bugs_fixed_in OWNER TO udd;

--
-- Name: bugs_found_in; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs_found_in (
    id integer NOT NULL,
    version text NOT NULL
);


ALTER TABLE public.bugs_found_in OWNER TO udd;

--
-- Name: bugs_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs_packages (
    id integer NOT NULL,
    package text NOT NULL,
    source text
);


ALTER TABLE public.bugs_packages OWNER TO udd;

--
-- Name: bugs_rt_affects_oldstable; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.bugs_rt_affects_oldstable AS
 SELECT bugs_rt_affects_dist.id,
    bugs_rt_affects_dist.package,
    bugs_rt_affects_dist.source
   FROM public.bugs_rt_affects_dist('oldstable'::text) bugs_rt_affects_dist(id, package, source);


ALTER TABLE public.bugs_rt_affects_oldstable OWNER TO udd;

--
-- Name: bugs_rt_affects_stable; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.bugs_rt_affects_stable AS
 SELECT bugs_rt_affects_dist.id,
    bugs_rt_affects_dist.package,
    bugs_rt_affects_dist.source
   FROM public.bugs_rt_affects_dist('stable'::text) bugs_rt_affects_dist(id, package, source);


ALTER TABLE public.bugs_rt_affects_stable OWNER TO udd;

--
-- Name: bugs_rt_affects_testing; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.bugs_rt_affects_testing AS
 SELECT bugs_rt_affects_dist.id,
    bugs_rt_affects_dist.package,
    bugs_rt_affects_dist.source
   FROM public.bugs_rt_affects_dist('testing'::text) bugs_rt_affects_dist(id, package, source);


ALTER TABLE public.bugs_rt_affects_testing OWNER TO udd;

--
-- Name: bugs_rt_affects_unstable; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.bugs_rt_affects_unstable AS
 SELECT bugs_rt_affects_dist.id,
    bugs_rt_affects_dist.package,
    bugs_rt_affects_dist.source
   FROM public.bugs_rt_affects_dist('unstable'::text) bugs_rt_affects_dist(id, package, source);


ALTER TABLE public.bugs_rt_affects_unstable OWNER TO udd;

--
-- Name: bugs_rt_affects_testing_and_unstable; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.bugs_rt_affects_testing_and_unstable AS
 SELECT bugs_rt_affects_testing.id,
    bugs_rt_affects_testing.package,
    bugs_rt_affects_testing.source
   FROM public.bugs_rt_affects_testing
  WHERE (bugs_rt_affects_testing.id IN ( SELECT bugs_rt_affects_unstable.id
           FROM public.bugs_rt_affects_unstable));


ALTER TABLE public.bugs_rt_affects_testing_and_unstable OWNER TO udd;

--
-- Name: bugs_stamps; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs_stamps (
    id integer NOT NULL,
    update_requested bigint,
    db_updated bigint
);


ALTER TABLE public.bugs_stamps OWNER TO udd;

--
-- Name: bugs_tags; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs_tags (
    id integer NOT NULL,
    tag text NOT NULL
);


ALTER TABLE public.bugs_tags OWNER TO udd;

--
-- Name: bugs_usertags; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.bugs_usertags (
    email text,
    tag text,
    id integer
);


ALTER TABLE public.bugs_usertags OWNER TO udd;

--
-- Name: carnivore_emails; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.carnivore_emails (
    id integer NOT NULL,
    email text NOT NULL
);


ALTER TABLE public.carnivore_emails OWNER TO udd;

--
-- Name: carnivore_names; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.carnivore_names (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.carnivore_names OWNER TO udd;

--
-- Name: ci; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ci (
    suite text NOT NULL,
    arch text NOT NULL,
    source text NOT NULL,
    version public.debversion,
    date timestamp without time zone,
    run_id text,
    status text,
    blame text,
    previous_status text,
    duration integer,
    message text
);


ALTER TABLE public.ci OWNER TO udd;

--
-- Name: debian_maintainers; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.debian_maintainers (
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    fingerprint text,
    package text,
    granted_by_fingerprint text
);


ALTER TABLE public.debian_maintainers OWNER TO udd;

--
-- Name: debtags; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.debtags (
    package text NOT NULL,
    tag text NOT NULL
);


ALTER TABLE public.debtags OWNER TO udd;

--
-- Name: deferred; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.deferred (
    source text NOT NULL,
    version public.debversion NOT NULL,
    distribution text,
    urgency text,
    date timestamp with time zone,
    delayed_until timestamp without time zone,
    delay_remaining interval,
    changed_by text,
    changed_by_name text,
    changed_by_email text,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    changes text
);


ALTER TABLE public.deferred OWNER TO udd;

--
-- Name: deferred_architecture; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.deferred_architecture (
    source text NOT NULL,
    version public.debversion NOT NULL,
    architecture text NOT NULL
);


ALTER TABLE public.deferred_architecture OWNER TO udd;

--
-- Name: deferred_binary; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.deferred_binary (
    source text NOT NULL,
    version public.debversion NOT NULL,
    package text NOT NULL
);


ALTER TABLE public.deferred_binary OWNER TO udd;

--
-- Name: deferred_closes; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.deferred_closes (
    source text NOT NULL,
    version public.debversion NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.deferred_closes OWNER TO udd;

--
-- Name: dehs; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.dehs (
    source text NOT NULL,
    unstable_version public.debversion,
    unstable_upstream text,
    unstable_parsed_version text,
    unstable_status public.dehs_status,
    unstable_last_uptodate timestamp without time zone,
    experimental_version public.debversion,
    experimental_upstream text,
    experimental_parsed_version text,
    experimental_status public.dehs_status,
    experimental_last_uptodate timestamp without time zone
);


ALTER TABLE public.dehs OWNER TO udd;

--
-- Name: derivatives_descriptions; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.derivatives_descriptions (
    package text NOT NULL,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    language text NOT NULL,
    description text NOT NULL,
    long_description text NOT NULL,
    description_md5 text NOT NULL
);


ALTER TABLE public.derivatives_descriptions OWNER TO udd;

--
-- Name: derivatives_packages_summary; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.derivatives_packages_summary (
    package text NOT NULL,
    version public.debversion NOT NULL,
    source text,
    source_version public.debversion,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL
);


ALTER TABLE public.derivatives_packages_summary OWNER TO udd;

--
-- Name: derivatives_uploaders; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.derivatives_uploaders (
    source text,
    version public.debversion,
    distribution text,
    release text,
    component text,
    uploader text,
    name text,
    email text
);


ALTER TABLE public.derivatives_uploaders OWNER TO udd;

--
-- Name: description_imports; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.description_imports (
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    language text NOT NULL,
    translationfile text NOT NULL,
    translationfile_sha1 text NOT NULL,
    import_date timestamp without time zone DEFAULT now()
);


ALTER TABLE public.description_imports OWNER TO udd;

--
-- Name: descriptions; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.descriptions (
    package text NOT NULL,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    language text NOT NULL,
    description text NOT NULL,
    long_description text NOT NULL,
    description_md5 text NOT NULL
);


ALTER TABLE public.descriptions OWNER TO udd;

--
-- Name: duck; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.duck (
    source text NOT NULL
);


ALTER TABLE public.duck OWNER TO udd;

--
-- Name: edam; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.edam (
    source text NOT NULL,
    package text NOT NULL,
    topics text[],
    scopes jsonb
);


ALTER TABLE public.edam OWNER TO udd;

--
-- Name: ftp_autorejects; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ftp_autorejects (
    tag text NOT NULL,
    autoreject_type public.ftp_autoreject_type,
    autoreject_level public.ftp_autoreject_level
);


ALTER TABLE public.ftp_autorejects OWNER TO udd;

--
-- Name: hints; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.hints (
    source text,
    version public.debversion,
    architecture text,
    type text,
    argument text,
    file text,
    comment text
);


ALTER TABLE public.hints OWNER TO udd;

--
-- Name: key_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.key_packages (
    source text NOT NULL,
    reason text
);


ALTER TABLE public.key_packages OWNER TO udd;

--
-- Name: ldap; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ldap (
    uid numeric NOT NULL,
    login text,
    cn text,
    sn text,
    expire boolean,
    location text,
    country text,
    activity_from timestamp with time zone,
    activity_from_info text,
    activity_pgp timestamp with time zone,
    activity_pgp_info text,
    gecos text,
    birthdate date,
    gender numeric,
    fingerprint text
);


ALTER TABLE public.ldap OWNER TO udd;

--
-- Name: lintian; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.lintian (
    package text NOT NULL,
    tag_type public.lintian_tagtype NOT NULL,
    package_type text,
    package_version public.debversion,
    package_arch text,
    tag text NOT NULL,
    information text
);


ALTER TABLE public.lintian OWNER TO udd;

--
-- Name: mentors_raw_uploads; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.mentors_raw_uploads (
    id numeric NOT NULL,
    changes text,
    closes text,
    component text,
    distribution text,
    package text,
    uploaded timestamp without time zone,
    uploader text,
    version text
);


ALTER TABLE public.mentors_raw_uploads OWNER TO udd;

--
-- Name: mentors_most_recent_package_versions; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.mentors_most_recent_package_versions AS
 SELECT mu.id,
    mu.changes,
    mu.closes,
    mu.component,
    mu.distribution,
    mu.package,
    mu.uploaded,
    mu.uploader,
    mu.version
   FROM (public.mentors_raw_uploads mu
     LEFT JOIN public.mentors_raw_uploads mu2 ON (((mu.package = mu2.package) AND (mu.uploaded < mu2.uploaded) AND (mu.distribution = mu2.distribution))))
  WHERE (mu2.id IS NULL);


ALTER TABLE public.mentors_most_recent_package_versions OWNER TO udd;

--
-- Name: migration_excuses; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.migration_excuses (
    item_name text NOT NULL,
    source text NOT NULL,
    migration_policy_verdict text,
    old_version public.debversion,
    new_version public.debversion,
    is_candidate boolean NOT NULL,
    excuses text[],
    reason text[],
    hints text[],
    policy_info text,
    dependencies text,
    invalidated_by_other_package boolean,
    missing_builds text,
    old_binaries text
);


ALTER TABLE public.migration_excuses OWNER TO udd;

--
-- Name: new_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.new_packages (
    package text NOT NULL,
    version text NOT NULL,
    architecture text NOT NULL,
    maintainer text,
    description text,
    source text,
    depends text,
    recommends text,
    suggests text,
    enhances text,
    pre_depends text,
    breaks text,
    replaces text,
    provides text,
    conflicts text,
    installed_size integer,
    homepage text,
    long_description text,
    section text,
    component text,
    distribution text,
    license text
);


ALTER TABLE public.new_packages OWNER TO udd;

--
-- Name: new_packages_madison; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.new_packages_madison AS
 SELECT new_packages.package,
    new_packages.version,
    new_packages.source,
    new_packages.distribution AS release,
    new_packages.architecture,
    new_packages.component,
    'debian'::text AS distribution
   FROM public.new_packages;


ALTER TABLE public.new_packages_madison OWNER TO udd;

--
-- Name: new_sources; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.new_sources (
    source text NOT NULL,
    version text NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    format text,
    files text,
    uploaders text,
    binaries text,
    changed_by text,
    architecture text,
    homepage text,
    vcs_type text,
    vcs_url text,
    vcs_browser text,
    section text,
    component text,
    distribution text NOT NULL,
    closes integer,
    license text,
    last_modified timestamp without time zone,
    queue text
);


ALTER TABLE public.new_sources OWNER TO udd;

--
-- Name: new_sources_madison; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.new_sources_madison AS
 SELECT new_sources.source,
    new_sources.version,
    new_sources.component,
    new_sources.architecture,
    new_sources.distribution AS release,
    'debian'::text AS distribution
   FROM public.new_sources;


ALTER TABLE public.new_sources_madison OWNER TO udd;

--
-- Name: package_removal; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.package_removal (
    batch_id integer NOT NULL,
    name text NOT NULL,
    version public.debversion NOT NULL,
    arch_array text[]
);


ALTER TABLE public.package_removal OWNER TO udd;

--
-- Name: package_removal_batch; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.package_removal_batch (
    id integer NOT NULL,
    "time" timestamp without time zone,
    ftpmaster text,
    distribution text,
    requestor text,
    reasons text
);


ALTER TABLE public.package_removal_batch OWNER TO udd;

--
-- Name: packages_summary; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.packages_summary (
    package text NOT NULL,
    version public.debversion NOT NULL,
    source text,
    source_version public.debversion,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL
);


ALTER TABLE public.packages_summary OWNER TO udd;

--
-- Name: piuparts_status; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.piuparts_status (
    section text,
    source text,
    version text,
    status text
);


ALTER TABLE public.piuparts_status OWNER TO udd;

--
-- Name: popcon; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.popcon (
    package text NOT NULL,
    insts integer,
    vote integer,
    olde integer,
    recent integer,
    nofiles integer
);


ALTER TABLE public.popcon OWNER TO udd;

--
-- Name: popcon_src_average; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.popcon_src_average (
    source text NOT NULL,
    insts integer,
    vote integer,
    olde integer,
    recent integer,
    nofiles integer
);


ALTER TABLE public.popcon_src_average OWNER TO udd;

--
-- Name: potential_bug_closures; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.potential_bug_closures (
    id integer,
    source text,
    distribution text,
    origin text
);


ALTER TABLE public.potential_bug_closures OWNER TO udd;

--
-- Name: pseudo_packages; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.pseudo_packages (
    package text NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    description text
);


ALTER TABLE public.pseudo_packages OWNER TO udd;

--
-- Name: registry; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.registry (
    source text NOT NULL,
    name text NOT NULL,
    entry text NOT NULL
);


ALTER TABLE public.registry OWNER TO udd;

--
-- Name: releases; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.releases (
    release text NOT NULL,
    releasedate date,
    role text,
    releaseversion text,
    distribution text,
    sort integer
);


ALTER TABLE public.releases OWNER TO udd;

--
-- Name: relevant_hints; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.relevant_hints AS
 SELECT hints.source,
    hints.version,
    hints.architecture,
    hints.type,
    hints.argument,
    hints.file,
    hints.comment
   FROM public.hints
  WHERE ((hints.version IS NULL) OR (hints.type = 'approve'::text) OR ((hints.type = ANY (ARRAY['unblock'::text, 'age-days'::text, 'hint'::text, 'easy'::text])) AND ((hints.source, (hints.version)::text) IN ( SELECT sources.source,
            sources.version
           FROM public.sources
          WHERE (sources.release = 'sid'::text)))) OR ((hints.type = 'remove'::text) AND ((hints.source, (hints.version)::text) IN ( SELECT sources.source,
            sources.version
           FROM public.sources
          WHERE (sources.release = 'squeeze'::text)))));


ALTER TABLE public.relevant_hints OWNER TO udd;

--
-- Name: reproducible; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.reproducible (
    source text NOT NULL,
    version public.debversion,
    release text NOT NULL,
    architecture text NOT NULL,
    status text
);


ALTER TABLE public.reproducible OWNER TO udd;

--
-- Name: screenshots; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.screenshots (
    package text NOT NULL,
    version text,
    homepage text,
    maintainer_name text,
    maintainer_email text,
    description text,
    section text,
    screenshot_url text NOT NULL,
    large_image_url text NOT NULL,
    small_image_url text NOT NULL
);


ALTER TABLE public.screenshots OWNER TO udd;

--
-- Name: security_issues; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.security_issues (
    source text NOT NULL,
    issue text NOT NULL,
    description text,
    scope public.security_issues_scope,
    bug integer
);


ALTER TABLE public.security_issues OWNER TO udd;

--
-- Name: security_issues_releases; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.security_issues_releases (
    source text NOT NULL,
    issue text NOT NULL,
    release text NOT NULL,
    fixed_version text,
    status public.security_issues_releases_status,
    urgency text,
    nodsa text,
    nodsa_reason text
);


ALTER TABLE public.security_issues_releases OWNER TO udd;

--
-- Name: sources_popcon; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.sources_popcon AS
 SELECT popcon_src.source,
    popcon_src.insts,
    popcon_src.vote,
    popcon_src.olde,
    popcon_src.recent,
    popcon_src.nofiles
   FROM public.popcon_src;


ALTER TABLE public.sources_popcon OWNER TO udd;

--
-- Name: sources_redundant; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.sources_redundant AS
 SELECT s1.source,
    s1.version,
    s1.maintainer,
    s1.maintainer_name,
    s1.maintainer_email,
    s1.format,
    s1.files,
    s1.uploaders,
    s1.bin,
    s1.architecture,
    s1.standards_version,
    s1.homepage,
    s1.build_depends,
    s1.build_depends_indep,
    s1.build_conflicts,
    s1.build_conflicts_indep,
    s1.priority,
    s1.section,
    s1.distribution,
    s1.release,
    s1.component,
    s1.vcs_type,
    s1.vcs_url,
    s1.vcs_browser,
    s1.python_version,
    s1.ruby_versions,
    s1.checksums_sha1,
    s1.checksums_sha256,
    s1.original_maintainer,
    s1.dm_upload_allowed,
    s1.testsuite,
    s1.autobuild,
    s1.extra_source_only
   FROM public.sources s1
  WHERE (EXISTS ( SELECT s2.source,
            s2.version,
            s2.maintainer,
            s2.maintainer_name,
            s2.maintainer_email,
            s2.format,
            s2.files,
            s2.uploaders,
            s2.bin,
            s2.architecture,
            s2.standards_version,
            s2.homepage,
            s2.build_depends,
            s2.build_depends_indep,
            s2.build_conflicts,
            s2.build_conflicts_indep,
            s2.priority,
            s2.section,
            s2.distribution,
            s2.release,
            s2.component,
            s2.vcs_type,
            s2.vcs_url,
            s2.vcs_browser,
            s2.python_version,
            s2.ruby_versions,
            s2.checksums_sha1,
            s2.checksums_sha256,
            s2.original_maintainer,
            s2.dm_upload_allowed,
            s2.testsuite,
            s2.autobuild,
            s2.extra_source_only
           FROM public.sources s2
          WHERE ((s1.source = s2.source) AND (s1.distribution = s2.distribution) AND (s1.release = s2.release) AND (s1.component = s2.component) AND (s2.version OPERATOR(public.>) s1.version))));


ALTER TABLE public.sources_redundant OWNER TO udd;

--
-- Name: sponsorship_requests; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.sponsorship_requests AS
 SELECT bugs.id,
    "substring"(bugs.title, '^RFS: ([^/]*)/'::text) AS source,
    "substring"(bugs.title, '/([^ ]*)( |$)'::text) AS version,
    bugs.title
   FROM public.bugs
  WHERE ((bugs.package = 'sponsorship-requests'::text) AND (bugs.status = 'pending'::text));


ALTER TABLE public.sponsorship_requests OWNER TO udd;

--
-- Name: testing_autoremovals; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.testing_autoremovals (
    source text,
    version text,
    bugs text,
    first_seen bigint,
    last_checked bigint,
    removal_time bigint,
    rdeps text,
    buggy_deps text,
    bugs_deps text,
    rdeps_popcon bigint
);


ALTER TABLE public.testing_autoremovals OWNER TO udd;

--
-- Name: timestamps; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.timestamps (
    id integer NOT NULL,
    source text,
    command text,
    start_time timestamp without time zone,
    end_time timestamp without time zone
);


ALTER TABLE public.timestamps OWNER TO udd;

--
-- Name: timestamps_id_seq; Type: SEQUENCE; Schema: public; Owner: udd
--

CREATE SEQUENCE public.timestamps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.timestamps_id_seq OWNER TO udd;

--
-- Name: timestamps_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: udd
--

ALTER SEQUENCE public.timestamps_id_seq OWNED BY public.timestamps.id;


--
-- Name: ubuntu_bugs; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_bugs (
    bug integer NOT NULL,
    title text,
    reporter_login text,
    reporter_name text,
    duplicate_of integer,
    date_reported text,
    date_updated text,
    security boolean,
    patches boolean
);


ALTER TABLE public.ubuntu_bugs OWNER TO udd;

--
-- Name: ubuntu_bugs_duplicates; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_bugs_duplicates (
    bug integer NOT NULL,
    duplicate integer NOT NULL
);


ALTER TABLE public.ubuntu_bugs_duplicates OWNER TO udd;

--
-- Name: ubuntu_bugs_subscribers; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_bugs_subscribers (
    bug integer,
    subscriber_login text,
    subscriber_name text
);


ALTER TABLE public.ubuntu_bugs_subscribers OWNER TO udd;

--
-- Name: ubuntu_bugs_tags; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_bugs_tags (
    bug integer NOT NULL,
    tag text NOT NULL
);


ALTER TABLE public.ubuntu_bugs_tags OWNER TO udd;

--
-- Name: ubuntu_bugs_tasks; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_bugs_tasks (
    bug integer NOT NULL,
    package text NOT NULL,
    distro text NOT NULL,
    status text,
    importance text,
    component text,
    milestone text,
    date_created text,
    date_assigned text,
    date_closed text,
    date_incomplete text,
    date_confirmed text,
    date_inprogress text,
    date_fix_committed text,
    date_fix_released text,
    date_left_new text,
    date_triaged text,
    date_left_closed text,
    watch text,
    reporter_login text,
    reporter_name text,
    assignee_login text,
    assignee_name text
);


ALTER TABLE public.ubuntu_bugs_tasks OWNER TO udd;

--
-- Name: ubuntu_description_imports; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_description_imports (
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    language text NOT NULL,
    translationfile text NOT NULL,
    translationfile_sha1 text NOT NULL,
    import_date timestamp without time zone DEFAULT now()
);


ALTER TABLE public.ubuntu_description_imports OWNER TO udd;

--
-- Name: ubuntu_descriptions; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_descriptions (
    package text NOT NULL,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    language text NOT NULL,
    description text NOT NULL,
    long_description text NOT NULL,
    description_md5 text NOT NULL
);


ALTER TABLE public.ubuntu_descriptions OWNER TO udd;

--
-- Name: ubuntu_packages_summary; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_packages_summary (
    package text NOT NULL,
    version public.debversion NOT NULL,
    source text,
    source_version public.debversion,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL
);


ALTER TABLE public.ubuntu_packages_summary OWNER TO udd;

--
-- Name: ubuntu_popcon; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_popcon (
    package text NOT NULL,
    insts integer,
    vote integer,
    olde integer,
    recent integer,
    nofiles integer
);


ALTER TABLE public.ubuntu_popcon OWNER TO udd;

--
-- Name: ubuntu_popcon_src; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_popcon_src (
    source text NOT NULL,
    insts integer,
    vote integer,
    olde integer,
    recent integer,
    nofiles integer
);


ALTER TABLE public.ubuntu_popcon_src OWNER TO udd;

--
-- Name: ubuntu_popcon_src_average; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_popcon_src_average (
    source text NOT NULL,
    insts integer,
    vote integer,
    olde integer,
    recent integer,
    nofiles integer
);


ALTER TABLE public.ubuntu_popcon_src_average OWNER TO udd;

--
-- Name: ubuntu_sources_popcon; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.ubuntu_sources_popcon AS
 SELECT ubuntu_popcon_src.source,
    ubuntu_popcon_src.insts,
    ubuntu_popcon_src.vote,
    ubuntu_popcon_src.olde,
    ubuntu_popcon_src.recent,
    ubuntu_popcon_src.nofiles
   FROM public.ubuntu_popcon_src;


ALTER TABLE public.ubuntu_sources_popcon OWNER TO udd;

--
-- Name: ubuntu_upload_history; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_upload_history (
    source text NOT NULL,
    version public.debversion NOT NULL,
    date timestamp with time zone,
    changed_by text,
    changed_by_name text,
    changed_by_email text,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    nmu boolean,
    signed_by text,
    signed_by_name text,
    signed_by_email text,
    key_id text,
    distribution text,
    component text,
    file text,
    fingerprint text,
    original_maintainer text,
    original_maintainer_name text,
    original_maintainer_email text
);


ALTER TABLE public.ubuntu_upload_history OWNER TO udd;

--
-- Name: ubuntu_upload_history_closes; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_upload_history_closes (
    source text NOT NULL,
    version public.debversion NOT NULL,
    bug integer NOT NULL,
    file text
);


ALTER TABLE public.ubuntu_upload_history_closes OWNER TO udd;

--
-- Name: ubuntu_upload_history_launchpad_closes; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_upload_history_launchpad_closes (
    source text NOT NULL,
    version public.debversion NOT NULL,
    bug integer NOT NULL,
    file text
);


ALTER TABLE public.ubuntu_upload_history_launchpad_closes OWNER TO udd;

--
-- Name: ubuntu_uploaders; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.ubuntu_uploaders (
    source text,
    version public.debversion,
    distribution text,
    release text,
    component text,
    uploader text,
    name text,
    email text
);


ALTER TABLE public.ubuntu_uploaders OWNER TO udd;

--
-- Name: udd_logs; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.udd_logs (
    importer text NOT NULL,
    "time" timestamp with time zone NOT NULL,
    duration integer,
    status integer,
    log text
);


ALTER TABLE public.udd_logs OWNER TO udd;

--
-- Name: upload_history_architecture; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.upload_history_architecture (
    source text NOT NULL,
    version public.debversion NOT NULL,
    architecture text NOT NULL,
    file text
);


ALTER TABLE public.upload_history_architecture OWNER TO udd;

--
-- Name: upload_history_closes; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.upload_history_closes (
    source text NOT NULL,
    version public.debversion NOT NULL,
    bug integer NOT NULL,
    file text
);


ALTER TABLE public.upload_history_closes OWNER TO udd;

--
-- Name: upstream; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.upstream (
    source text NOT NULL,
    version public.debversion NOT NULL,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    watch_file text,
    signing_key_pgp bytea,
    signing_key_asc text,
    debian_uversion text,
    debian_mangled_uversion text,
    upstream_version text,
    upstream_url text,
    errors text,
    warnings text,
    status text,
    last_check timestamp without time zone
);


ALTER TABLE public.upstream OWNER TO udd;

--
-- Name: upstream_metadata; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.upstream_metadata (
    source text NOT NULL,
    key text NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.upstream_metadata OWNER TO udd;

--
-- Name: upstream_status; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.upstream_status (
    source text,
    version public.debversion,
    distribution text,
    release text,
    component text,
    watch_file text,
    debian_uversion text,
    debian_mangled_uversion text,
    upstream_version text,
    upstream_url text,
    errors text,
    warnings text,
    status text,
    last_check timestamp without time zone
);


ALTER TABLE public.upstream_status OWNER TO udd;

--
-- Name: vcs; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.vcs (
    source text NOT NULL,
    team text,
    version public.debversion,
    distribution text
);


ALTER TABLE public.vcs OWNER TO udd;

--
-- Name: vcswatch; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.vcswatch (
    source text NOT NULL,
    version public.debversion,
    vcs text,
    url text,
    branch text,
    browser text,
    last_scan timestamp with time zone,
    next_scan timestamp with time zone DEFAULT now(),
    status text DEFAULT 'TODO'::text,
    debian_dir boolean DEFAULT true,
    changelog_version public.debversion,
    changelog_distribution text,
    changelog text,
    error text,
    commit_id text
);


ALTER TABLE public.vcswatch OWNER TO udd;

--
-- Name: wannabuild; Type: TABLE; Schema: public; Owner: udd
--

CREATE TABLE public.wannabuild (
    source text NOT NULL,
    distribution text NOT NULL,
    architecture text NOT NULL,
    version public.debversion,
    state text,
    installed_version public.debversion,
    previous_state text,
    state_change timestamp without time zone,
    binary_nmu_version numeric,
    notes text,
    vancouvered boolean
);


ALTER TABLE public.wannabuild OWNER TO udd;

--
-- Name: wnpp; Type: VIEW; Schema: public; Owner: udd
--

CREATE VIEW public.wnpp AS
 SELECT bugs.id,
    "substring"(bugs.title, '^([A-Z]{1,3}): .*'::text) AS type,
    "substring"(bugs.title, '^[A-Z]{1,3}: ([^ ]+)(?: -- .*)'::text) AS source,
    bugs.title
   FROM public.bugs
  WHERE ((bugs.package = 'wnpp'::text) AND (bugs.status <> 'done'::text));


ALTER TABLE public.wnpp OWNER TO udd;

--
-- Name: timestamps id; Type: DEFAULT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.timestamps ALTER COLUMN id SET DEFAULT nextval('public.timestamps_id_seq'::regclass);


--
-- Name: sources_count sources_count_pkey; Type: CONSTRAINT; Schema: history; Owner: udd
--

ALTER TABLE ONLY history.sources_count
    ADD CONSTRAINT sources_count_pkey PRIMARY KEY (ts);


--
-- Name: archived_bugs_blockedby archived_bugs_blockedby_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_blockedby
    ADD CONSTRAINT archived_bugs_blockedby_pkey PRIMARY KEY (id, blocker);


--
-- Name: archived_bugs_blocks archived_bugs_blocks_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_blocks
    ADD CONSTRAINT archived_bugs_blocks_pkey PRIMARY KEY (id, blocked);


--
-- Name: archived_bugs_fixed_in archived_bugs_fixed_in_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_fixed_in
    ADD CONSTRAINT archived_bugs_fixed_in_pkey PRIMARY KEY (id, version);


--
-- Name: archived_bugs_found_in archived_bugs_found_in_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_found_in
    ADD CONSTRAINT archived_bugs_found_in_pkey PRIMARY KEY (id, version);


--
-- Name: archived_bugs_merged_with archived_bugs_merged_with_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_merged_with
    ADD CONSTRAINT archived_bugs_merged_with_pkey PRIMARY KEY (id, merged_with);


--
-- Name: archived_bugs_packages archived_bugs_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_packages
    ADD CONSTRAINT archived_bugs_packages_pkey PRIMARY KEY (id, package);


--
-- Name: archived_bugs archived_bugs_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs
    ADD CONSTRAINT archived_bugs_pkey PRIMARY KEY (id);


--
-- Name: archived_bugs_stamps archived_bugs_stamps_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_stamps
    ADD CONSTRAINT archived_bugs_stamps_pkey PRIMARY KEY (id);


--
-- Name: archived_bugs_tags archived_bugs_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_tags
    ADD CONSTRAINT archived_bugs_tags_pkey PRIMARY KEY (id, tag);


--
-- Name: archived_descriptions archived_descriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_descriptions
    ADD CONSTRAINT archived_descriptions_pkey PRIMARY KEY (package, distribution, release, component, language, description, description_md5);


--
-- Name: archived_packages archived_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_packages
    ADD CONSTRAINT archived_packages_pkey PRIMARY KEY (package, version, architecture, distribution, release, component);


--
-- Name: archived_packages_summary archived_packages_summary_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_packages_summary
    ADD CONSTRAINT archived_packages_summary_pkey PRIMARY KEY (package, version, distribution, release, component);


--
-- Name: archived_sources archived_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_sources
    ADD CONSTRAINT archived_sources_pkey PRIMARY KEY (source, version, distribution, release, component);


--
-- Name: bibref bibref_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bibref
    ADD CONSTRAINT bibref_pkey PRIMARY KEY (source, key, package, rank);


--
-- Name: blends_dependencies_alternatives blends_dependencies_alternatives_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_dependencies_alternatives
    ADD CONSTRAINT blends_dependencies_alternatives_pkey PRIMARY KEY (blend, task, alternatives);


--
-- Name: blends_dependencies blends_dependencies_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_dependencies
    ADD CONSTRAINT blends_dependencies_pkey PRIMARY KEY (blend, task, package);


--
-- Name: blends_metadata blends_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_metadata
    ADD CONSTRAINT blends_metadata_pkey PRIMARY KEY (blend);


--
-- Name: blends_prospectivepackages blends_prospectivepackages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_prospectivepackages
    ADD CONSTRAINT blends_prospectivepackages_pkey PRIMARY KEY (package);


--
-- Name: blends_remarks blends_remarks_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_remarks
    ADD CONSTRAINT blends_remarks_pkey PRIMARY KEY (blend, task, package);


--
-- Name: blends_tasks blends_tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_tasks
    ADD CONSTRAINT blends_tasks_pkey PRIMARY KEY (blend, task);


--
-- Name: blends_unknown_packages blends_unknown_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_unknown_packages
    ADD CONSTRAINT blends_unknown_packages_pkey PRIMARY KEY (package);


--
-- Name: bugs_blockedby bugs_blockedby_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_blockedby
    ADD CONSTRAINT bugs_blockedby_pkey PRIMARY KEY (id, blocker);


--
-- Name: bugs_blocks bugs_blocks_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_blocks
    ADD CONSTRAINT bugs_blocks_pkey PRIMARY KEY (id, blocked);


--
-- Name: bugs_fixed_in bugs_fixed_in_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_fixed_in
    ADD CONSTRAINT bugs_fixed_in_pkey PRIMARY KEY (id, version);


--
-- Name: bugs_found_in bugs_found_in_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_found_in
    ADD CONSTRAINT bugs_found_in_pkey PRIMARY KEY (id, version);


--
-- Name: bugs_merged_with bugs_merged_with_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_merged_with
    ADD CONSTRAINT bugs_merged_with_pkey PRIMARY KEY (id, merged_with);


--
-- Name: bugs_packages bugs_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_packages
    ADD CONSTRAINT bugs_packages_pkey PRIMARY KEY (id, package);


--
-- Name: bugs bugs_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs
    ADD CONSTRAINT bugs_pkey PRIMARY KEY (id);


--
-- Name: bugs_stamps bugs_stamps_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_stamps
    ADD CONSTRAINT bugs_stamps_pkey PRIMARY KEY (id);


--
-- Name: bugs_tags bugs_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_tags
    ADD CONSTRAINT bugs_tags_pkey PRIMARY KEY (id, tag);


--
-- Name: carnivore_emails carnivore_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.carnivore_emails
    ADD CONSTRAINT carnivore_emails_pkey PRIMARY KEY (id, email);


--
-- Name: carnivore_keys carnivore_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.carnivore_keys
    ADD CONSTRAINT carnivore_keys_pkey PRIMARY KEY (key, key_type);


--
-- Name: carnivore_login carnivore_login_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.carnivore_login
    ADD CONSTRAINT carnivore_login_pkey PRIMARY KEY (id);


--
-- Name: carnivore_names carnivore_names_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.carnivore_names
    ADD CONSTRAINT carnivore_names_pkey PRIMARY KEY (id, name);


--
-- Name: ci ci_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ci
    ADD CONSTRAINT ci_pkey PRIMARY KEY (suite, arch, source);


--
-- Name: deferred_architecture deferred_architecture_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.deferred_architecture
    ADD CONSTRAINT deferred_architecture_pkey PRIMARY KEY (source, version, architecture);


--
-- Name: deferred_binary deferred_binary_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.deferred_binary
    ADD CONSTRAINT deferred_binary_pkey PRIMARY KEY (source, version, package);


--
-- Name: deferred_closes deferred_closes_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.deferred_closes
    ADD CONSTRAINT deferred_closes_pkey PRIMARY KEY (source, version, id);


--
-- Name: deferred deferred_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.deferred
    ADD CONSTRAINT deferred_pkey PRIMARY KEY (source, version);


--
-- Name: dehs dehs_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.dehs
    ADD CONSTRAINT dehs_pkey PRIMARY KEY (source);


--
-- Name: derivatives_descriptions derivatives_descriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.derivatives_descriptions
    ADD CONSTRAINT derivatives_descriptions_pkey PRIMARY KEY (package, distribution, release, component, language, description, description_md5);


--
-- Name: derivatives_packages derivatives_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.derivatives_packages
    ADD CONSTRAINT derivatives_packages_pkey PRIMARY KEY (package, version, architecture, distribution, release, component);


--
-- Name: derivatives_packages_summary derivatives_packages_summary_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.derivatives_packages_summary
    ADD CONSTRAINT derivatives_packages_summary_pkey PRIMARY KEY (package, version, distribution, release, component);


--
-- Name: derivatives_sources derivatives_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.derivatives_sources
    ADD CONSTRAINT derivatives_sources_pkey PRIMARY KEY (source, version, distribution, release, component);


--
-- Name: description_imports description_imports_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.description_imports
    ADD CONSTRAINT description_imports_pkey PRIMARY KEY (distribution, release, component, language);


--
-- Name: descriptions descriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.descriptions
    ADD CONSTRAINT descriptions_pkey PRIMARY KEY (package, distribution, release, component, language, description, description_md5);


--
-- Name: duck duck_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.duck
    ADD CONSTRAINT duck_pkey PRIMARY KEY (source);


--
-- Name: edam edam_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.edam
    ADD CONSTRAINT edam_pkey PRIMARY KEY (source, package);


--
-- Name: ftp_autorejects ftp_autorejects_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ftp_autorejects
    ADD CONSTRAINT ftp_autorejects_pkey PRIMARY KEY (tag);


--
-- Name: key_packages key_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.key_packages
    ADD CONSTRAINT key_packages_pkey PRIMARY KEY (source);


--
-- Name: ldap ldap_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ldap
    ADD CONSTRAINT ldap_pkey PRIMARY KEY (uid);


--
-- Name: mentors_raw_uploads mentors_raw_uploads_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.mentors_raw_uploads
    ADD CONSTRAINT mentors_raw_uploads_pkey PRIMARY KEY (id);


--
-- Name: migration_excuses migration_excuses_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.migration_excuses
    ADD CONSTRAINT migration_excuses_pkey PRIMARY KEY (item_name);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (source);


--
-- Name: new_packages new_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.new_packages
    ADD CONSTRAINT new_packages_pkey PRIMARY KEY (package, version, architecture);


--
-- Name: new_sources new_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.new_sources
    ADD CONSTRAINT new_sources_pkey PRIMARY KEY (source, version, distribution);


--
-- Name: orphaned_packages orphaned_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.orphaned_packages
    ADD CONSTRAINT orphaned_packages_pkey PRIMARY KEY (source);


--
-- Name: package_removal_batch package_removal_batch_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.package_removal_batch
    ADD CONSTRAINT package_removal_batch_pkey PRIMARY KEY (id);


--
-- Name: package_removal package_removal_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.package_removal
    ADD CONSTRAINT package_removal_pkey PRIMARY KEY (batch_id, name, version);


--
-- Name: packages packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.packages
    ADD CONSTRAINT packages_pkey PRIMARY KEY (package, version, architecture, distribution, release, component);


--
-- Name: packages_summary packages_summary_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.packages_summary
    ADD CONSTRAINT packages_summary_pkey PRIMARY KEY (package, version, distribution, release, component);


--
-- Name: popcon popcon_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.popcon
    ADD CONSTRAINT popcon_pkey PRIMARY KEY (package);


--
-- Name: popcon_src_average popcon_src_average_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.popcon_src_average
    ADD CONSTRAINT popcon_src_average_pkey PRIMARY KEY (source);


--
-- Name: popcon_src popcon_src_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.popcon_src
    ADD CONSTRAINT popcon_src_pkey PRIMARY KEY (source);


--
-- Name: pseudo_packages pseudo_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.pseudo_packages
    ADD CONSTRAINT pseudo_packages_pkey PRIMARY KEY (package);


--
-- Name: registry registry_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.registry
    ADD CONSTRAINT registry_pkey PRIMARY KEY (source, name, entry);


--
-- Name: releases releases_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.releases
    ADD CONSTRAINT releases_pkey PRIMARY KEY (release);


--
-- Name: reproducible reproducible_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.reproducible
    ADD CONSTRAINT reproducible_pkey PRIMARY KEY (source, release, architecture);


--
-- Name: screenshots screenshots_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.screenshots
    ADD CONSTRAINT screenshots_pkey PRIMARY KEY (small_image_url);


--
-- Name: security_issues security_issues_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.security_issues
    ADD CONSTRAINT security_issues_pkey PRIMARY KEY (source, issue);


--
-- Name: security_issues_releases security_issues_releases_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.security_issues_releases
    ADD CONSTRAINT security_issues_releases_pkey PRIMARY KEY (source, issue, release);


--
-- Name: sources sources_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT sources_pkey PRIMARY KEY (source, version, distribution, release, component);


--
-- Name: timestamps timestamps_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.timestamps
    ADD CONSTRAINT timestamps_pkey PRIMARY KEY (id);


--
-- Name: ubuntu_bugs_duplicates ubuntu_bugs_duplicates_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_bugs_duplicates
    ADD CONSTRAINT ubuntu_bugs_duplicates_pkey PRIMARY KEY (bug, duplicate);


--
-- Name: ubuntu_bugs ubuntu_bugs_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_bugs
    ADD CONSTRAINT ubuntu_bugs_pkey PRIMARY KEY (bug);


--
-- Name: ubuntu_bugs_tags ubuntu_bugs_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_bugs_tags
    ADD CONSTRAINT ubuntu_bugs_tags_pkey PRIMARY KEY (bug, tag);


--
-- Name: ubuntu_bugs_tasks ubuntu_bugs_tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_bugs_tasks
    ADD CONSTRAINT ubuntu_bugs_tasks_pkey PRIMARY KEY (bug, package, distro);


--
-- Name: ubuntu_description_imports ubuntu_description_imports_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_description_imports
    ADD CONSTRAINT ubuntu_description_imports_pkey PRIMARY KEY (distribution, release, component, language);


--
-- Name: ubuntu_descriptions ubuntu_descriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_descriptions
    ADD CONSTRAINT ubuntu_descriptions_pkey PRIMARY KEY (package, distribution, release, component, language, description, description_md5);


--
-- Name: ubuntu_packages ubuntu_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_packages
    ADD CONSTRAINT ubuntu_packages_pkey PRIMARY KEY (package, version, architecture, distribution, release, component);


--
-- Name: ubuntu_packages_summary ubuntu_packages_summary_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_packages_summary
    ADD CONSTRAINT ubuntu_packages_summary_pkey PRIMARY KEY (package, version, distribution, release, component);


--
-- Name: ubuntu_popcon ubuntu_popcon_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_popcon
    ADD CONSTRAINT ubuntu_popcon_pkey PRIMARY KEY (package);


--
-- Name: ubuntu_popcon_src_average ubuntu_popcon_src_average_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_popcon_src_average
    ADD CONSTRAINT ubuntu_popcon_src_average_pkey PRIMARY KEY (source);


--
-- Name: ubuntu_popcon_src ubuntu_popcon_src_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_popcon_src
    ADD CONSTRAINT ubuntu_popcon_src_pkey PRIMARY KEY (source);


--
-- Name: ubuntu_sources ubuntu_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_sources
    ADD CONSTRAINT ubuntu_sources_pkey PRIMARY KEY (source, version, distribution, release, component);


--
-- Name: ubuntu_upload_history_closes ubuntu_upload_history_closes_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_upload_history_closes
    ADD CONSTRAINT ubuntu_upload_history_closes_pkey PRIMARY KEY (source, version, bug);


--
-- Name: ubuntu_upload_history_launchpad_closes ubuntu_upload_history_launchpad_closes_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_upload_history_launchpad_closes
    ADD CONSTRAINT ubuntu_upload_history_launchpad_closes_pkey PRIMARY KEY (source, version, bug);


--
-- Name: ubuntu_upload_history ubuntu_upload_history_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_upload_history
    ADD CONSTRAINT ubuntu_upload_history_pkey PRIMARY KEY (source, version);


--
-- Name: udd_logs udd_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.udd_logs
    ADD CONSTRAINT udd_logs_pkey PRIMARY KEY (importer, "time");


--
-- Name: upload_history_architecture upload_history_architecture_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.upload_history_architecture
    ADD CONSTRAINT upload_history_architecture_pkey PRIMARY KEY (source, version, architecture);


--
-- Name: upload_history_closes upload_history_closes_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.upload_history_closes
    ADD CONSTRAINT upload_history_closes_pkey PRIMARY KEY (source, version, bug);


--
-- Name: upload_history upload_history_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.upload_history
    ADD CONSTRAINT upload_history_pkey PRIMARY KEY (source, version);


--
-- Name: upstream_metadata upstream_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.upstream_metadata
    ADD CONSTRAINT upstream_metadata_pkey PRIMARY KEY (source, key);


--
-- Name: upstream upstream_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.upstream
    ADD CONSTRAINT upstream_pkey PRIMARY KEY (source, version, distribution, release, component);


--
-- Name: vcs vcs_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.vcs
    ADD CONSTRAINT vcs_pkey PRIMARY KEY (source);


--
-- Name: vcswatch vcswatch_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.vcswatch
    ADD CONSTRAINT vcswatch_pkey PRIMARY KEY (source);


--
-- Name: wannabuild wannabuild_pkey; Type: CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.wannabuild
    ADD CONSTRAINT wannabuild_pkey PRIMARY KEY (source, distribution, architecture);


--
-- Name: archived_packages_distrelcomp_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX archived_packages_distrelcomp_idx ON public.archived_packages USING btree (distribution, release, component);


--
-- Name: archived_packages_source_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX archived_packages_source_idx ON public.archived_packages USING btree (source);


--
-- Name: archived_packages_summary_distrelcompsrcver_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX archived_packages_summary_distrelcompsrcver_idx ON public.archived_packages_summary USING btree (distribution, release, component, source, source_version);


--
-- Name: archived_sources_distrelcomp_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX archived_sources_distrelcomp_idx ON public.archived_sources USING btree (distribution, release, component);


--
-- Name: archived_uploaders_distrelcompsrcver_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX archived_uploaders_distrelcompsrcver_idx ON public.archived_uploaders USING btree (distribution, release, component, source, version);


--
-- Name: bugs_package_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX bugs_package_idx ON public.bugs USING btree (package);


--
-- Name: bugs_packages_package_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX bugs_packages_package_idx ON public.bugs_packages USING btree (package);


--
-- Name: bugs_packages_source_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX bugs_packages_source_idx ON public.bugs_packages USING btree (source);


--
-- Name: bugs_severity_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX bugs_severity_idx ON public.bugs USING btree (severity);


--
-- Name: bugs_source_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX bugs_source_idx ON public.bugs USING btree (source);


--
-- Name: bugs_tags_tag_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX bugs_tags_tag_idx ON public.bugs_tags USING btree (tag);


--
-- Name: carnivore_keys_id_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX carnivore_keys_id_idx ON public.carnivore_keys USING btree (id);


--
-- Name: debtags_package_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX debtags_package_idx ON public.debtags USING btree (package);


--
-- Name: debtags_tag_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX debtags_tag_idx ON public.debtags USING btree (tag);


--
-- Name: derivatives_packages_distrelcomp_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX derivatives_packages_distrelcomp_idx ON public.derivatives_packages USING btree (distribution, release, component);


--
-- Name: derivatives_packages_source_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX derivatives_packages_source_idx ON public.derivatives_packages USING btree (source);


--
-- Name: derivatives_packages_summary_distrelcompsrcver_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX derivatives_packages_summary_distrelcompsrcver_idx ON public.derivatives_packages_summary USING btree (distribution, release, component, source, source_version);


--
-- Name: derivatives_sources_distrelcomp_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX derivatives_sources_distrelcomp_idx ON public.derivatives_sources USING btree (distribution, release, component);


--
-- Name: derivatives_uploaders_distrelcompsrcver_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX derivatives_uploaders_distrelcompsrcver_idx ON public.derivatives_uploaders USING btree (distribution, release, component, source, version);


--
-- Name: hints_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX hints_idx ON public.hints USING btree (source, version);


--
-- Name: lintian_package_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX lintian_package_idx ON public.lintian USING btree (package);


--
-- Name: packages_distrelcomp_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX packages_distrelcomp_idx ON public.packages USING btree (distribution, release, component);


--
-- Name: packages_pkgverdescr_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX packages_pkgverdescr_idx ON public.packages USING btree (package, version, description);


--
-- Name: packages_source_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX packages_source_idx ON public.packages USING btree (source);


--
-- Name: packages_summary_distrelcompsrcver_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX packages_summary_distrelcompsrcver_idx ON public.packages_summary USING btree (distribution, release, component, source, source_version);


--
-- Name: piuparts_status_section_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX piuparts_status_section_idx ON public.piuparts_status USING btree (section);


--
-- Name: piuparts_status_source_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX piuparts_status_source_idx ON public.piuparts_status USING btree (source);


--
-- Name: piuparts_status_status_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX piuparts_status_status_idx ON public.piuparts_status USING btree (status);


--
-- Name: potential_bug_closures_id_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX potential_bug_closures_id_idx ON public.potential_bug_closures USING btree (id);


--
-- Name: potential_bug_closures_source_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX potential_bug_closures_source_idx ON public.potential_bug_closures USING btree (source);


--
-- Name: sources_distrelcomp_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX sources_distrelcomp_idx ON public.sources USING btree (distribution, release, component);


--
-- Name: sources_release_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX sources_release_idx ON public.sources USING btree (release);


--
-- Name: testing_autoremovals_source_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX testing_autoremovals_source_idx ON public.testing_autoremovals USING btree (source);


--
-- Name: ubuntu_bugs_duplicates_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_bugs_duplicates_idx ON public.ubuntu_bugs_duplicates USING btree (bug);


--
-- Name: ubuntu_bugs_subscribers_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_bugs_subscribers_idx ON public.ubuntu_bugs_subscribers USING btree (bug);


--
-- Name: ubuntu_bugs_tags_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_bugs_tags_idx ON public.ubuntu_bugs_tags USING btree (bug);


--
-- Name: ubuntu_bugs_tasks_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_bugs_tasks_idx ON public.ubuntu_bugs_tasks USING btree (bug);


--
-- Name: ubuntu_bugs_tasks_package_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_bugs_tasks_package_idx ON public.ubuntu_bugs_tasks USING btree (package);


--
-- Name: ubuntu_packages_distrelcomp_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_packages_distrelcomp_idx ON public.ubuntu_packages USING btree (distribution, release, component);


--
-- Name: ubuntu_packages_source_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_packages_source_idx ON public.ubuntu_packages USING btree (source);


--
-- Name: ubuntu_packages_summary_distrelcompsrcver_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_packages_summary_distrelcompsrcver_idx ON public.ubuntu_packages_summary USING btree (distribution, release, component, source, source_version);


--
-- Name: ubuntu_sources_distrelcomp_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_sources_distrelcomp_idx ON public.ubuntu_sources USING btree (distribution, release, component);


--
-- Name: ubuntu_uploaders_distrelcompsrcver_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX ubuntu_uploaders_distrelcompsrcver_idx ON public.ubuntu_uploaders USING btree (distribution, release, component, source, version);


--
-- Name: upload_history_distribution_date_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX upload_history_distribution_date_idx ON public.upload_history USING btree (distribution, date);


--
-- Name: upload_history_fingerprint_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX upload_history_fingerprint_idx ON public.upload_history USING btree (fingerprint);


--
-- Name: uploaders_distrelcompsrcver_idx; Type: INDEX; Schema: public; Owner: udd
--

CREATE INDEX uploaders_distrelcompsrcver_idx ON public.uploaders USING btree (distribution, release, component, source, version);


--
-- Name: archived_bugs_blockedby archived_bugs_blockedby_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_blockedby
    ADD CONSTRAINT archived_bugs_blockedby_id_fkey FOREIGN KEY (id) REFERENCES public.archived_bugs(id);


--
-- Name: archived_bugs_blocks archived_bugs_blocks_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_blocks
    ADD CONSTRAINT archived_bugs_blocks_id_fkey FOREIGN KEY (id) REFERENCES public.archived_bugs(id);


--
-- Name: archived_bugs_fixed_in archived_bugs_fixed_in_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_fixed_in
    ADD CONSTRAINT archived_bugs_fixed_in_id_fkey FOREIGN KEY (id) REFERENCES public.archived_bugs(id);


--
-- Name: archived_bugs_found_in archived_bugs_found_in_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_found_in
    ADD CONSTRAINT archived_bugs_found_in_id_fkey FOREIGN KEY (id) REFERENCES public.archived_bugs(id);


--
-- Name: archived_bugs_merged_with archived_bugs_merged_with_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_merged_with
    ADD CONSTRAINT archived_bugs_merged_with_id_fkey FOREIGN KEY (id) REFERENCES public.archived_bugs(id);


--
-- Name: archived_bugs_packages archived_bugs_packages_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_packages
    ADD CONSTRAINT archived_bugs_packages_id_fkey FOREIGN KEY (id) REFERENCES public.archived_bugs(id);


--
-- Name: archived_bugs_tags archived_bugs_tags_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_bugs_tags
    ADD CONSTRAINT archived_bugs_tags_id_fkey FOREIGN KEY (id) REFERENCES public.archived_bugs(id);


--
-- Name: archived_packages archived_packages_package_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.archived_packages
    ADD CONSTRAINT archived_packages_package_fkey FOREIGN KEY (package, version, distribution, release, component) REFERENCES public.archived_packages_summary(package, version, distribution, release, component) DEFERRABLE;


--
-- Name: blends_dependencies_alternatives blends_dependencies_alternatives_blend_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_dependencies_alternatives
    ADD CONSTRAINT blends_dependencies_alternatives_blend_fkey FOREIGN KEY (blend) REFERENCES public.blends_metadata(blend);


--
-- Name: blends_dependencies blends_dependencies_blend_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_dependencies
    ADD CONSTRAINT blends_dependencies_blend_fkey FOREIGN KEY (blend) REFERENCES public.blends_metadata(blend);


--
-- Name: blends_tasks blends_tasks_blend_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.blends_tasks
    ADD CONSTRAINT blends_tasks_blend_fkey FOREIGN KEY (blend) REFERENCES public.blends_metadata(blend);


--
-- Name: bugs_blockedby bugs_blockedby_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_blockedby
    ADD CONSTRAINT bugs_blockedby_id_fkey FOREIGN KEY (id) REFERENCES public.bugs(id);


--
-- Name: bugs_blocks bugs_blocks_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_blocks
    ADD CONSTRAINT bugs_blocks_id_fkey FOREIGN KEY (id) REFERENCES public.bugs(id);


--
-- Name: bugs_fixed_in bugs_fixed_in_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_fixed_in
    ADD CONSTRAINT bugs_fixed_in_id_fkey FOREIGN KEY (id) REFERENCES public.bugs(id);


--
-- Name: bugs_found_in bugs_found_in_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_found_in
    ADD CONSTRAINT bugs_found_in_id_fkey FOREIGN KEY (id) REFERENCES public.bugs(id);


--
-- Name: bugs_merged_with bugs_merged_with_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_merged_with
    ADD CONSTRAINT bugs_merged_with_id_fkey FOREIGN KEY (id) REFERENCES public.bugs(id);


--
-- Name: bugs_packages bugs_packages_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_packages
    ADD CONSTRAINT bugs_packages_id_fkey FOREIGN KEY (id) REFERENCES public.bugs(id);


--
-- Name: bugs_tags bugs_tags_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.bugs_tags
    ADD CONSTRAINT bugs_tags_id_fkey FOREIGN KEY (id) REFERENCES public.bugs(id);


--
-- Name: deferred_architecture deferred_architecture_source_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.deferred_architecture
    ADD CONSTRAINT deferred_architecture_source_fkey FOREIGN KEY (source, version) REFERENCES public.deferred(source, version) DEFERRABLE;


--
-- Name: deferred_binary deferred_binary_source_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.deferred_binary
    ADD CONSTRAINT deferred_binary_source_fkey FOREIGN KEY (source, version) REFERENCES public.deferred(source, version) DEFERRABLE;


--
-- Name: deferred_closes deferred_closes_source_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.deferred_closes
    ADD CONSTRAINT deferred_closes_source_fkey FOREIGN KEY (source, version) REFERENCES public.deferred(source, version) DEFERRABLE;


--
-- Name: derivatives_packages derivatives_packages_package_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.derivatives_packages
    ADD CONSTRAINT derivatives_packages_package_fkey FOREIGN KEY (package, version, distribution, release, component) REFERENCES public.derivatives_packages_summary(package, version, distribution, release, component) DEFERRABLE;


--
-- Name: package_removal package_removal_batch_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.package_removal
    ADD CONSTRAINT package_removal_batch_id_fkey FOREIGN KEY (batch_id) REFERENCES public.package_removal_batch(id);


--
-- Name: packages packages_package_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.packages
    ADD CONSTRAINT packages_package_fkey FOREIGN KEY (package, version, distribution, release, component) REFERENCES public.packages_summary(package, version, distribution, release, component) DEFERRABLE;


--
-- Name: security_issues_releases security_issues_releases_source_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.security_issues_releases
    ADD CONSTRAINT security_issues_releases_source_fkey FOREIGN KEY (source, issue) REFERENCES public.security_issues(source, issue) DEFERRABLE;


--
-- Name: ubuntu_bugs_duplicates ubuntu_bugs_duplicates_bug_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_bugs_duplicates
    ADD CONSTRAINT ubuntu_bugs_duplicates_bug_fkey FOREIGN KEY (bug) REFERENCES public.ubuntu_bugs(bug);


--
-- Name: ubuntu_bugs_subscribers ubuntu_bugs_subscribers_bug_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_bugs_subscribers
    ADD CONSTRAINT ubuntu_bugs_subscribers_bug_fkey FOREIGN KEY (bug) REFERENCES public.ubuntu_bugs(bug);


--
-- Name: ubuntu_bugs_tags ubuntu_bugs_tags_bug_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_bugs_tags
    ADD CONSTRAINT ubuntu_bugs_tags_bug_fkey FOREIGN KEY (bug) REFERENCES public.ubuntu_bugs(bug);


--
-- Name: ubuntu_bugs_tasks ubuntu_bugs_tasks_bug_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_bugs_tasks
    ADD CONSTRAINT ubuntu_bugs_tasks_bug_fkey FOREIGN KEY (bug) REFERENCES public.ubuntu_bugs(bug);


--
-- Name: ubuntu_packages ubuntu_packages_package_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_packages
    ADD CONSTRAINT ubuntu_packages_package_fkey FOREIGN KEY (package, version, distribution, release, component) REFERENCES public.ubuntu_packages_summary(package, version, distribution, release, component) DEFERRABLE;


--
-- Name: ubuntu_upload_history_closes ubuntu_upload_history_closes_source_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_upload_history_closes
    ADD CONSTRAINT ubuntu_upload_history_closes_source_fkey FOREIGN KEY (source, version) REFERENCES public.ubuntu_upload_history(source, version) DEFERRABLE;


--
-- Name: ubuntu_upload_history_launchpad_closes ubuntu_upload_history_launchpad_closes_source_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.ubuntu_upload_history_launchpad_closes
    ADD CONSTRAINT ubuntu_upload_history_launchpad_closes_source_fkey FOREIGN KEY (source, version) REFERENCES public.ubuntu_upload_history(source, version) DEFERRABLE;


--
-- Name: upload_history_architecture upload_history_architecture_source_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.upload_history_architecture
    ADD CONSTRAINT upload_history_architecture_source_fkey FOREIGN KEY (source, version) REFERENCES public.upload_history(source, version) DEFERRABLE;


--
-- Name: upload_history_closes upload_history_closes_source_fkey; Type: FK CONSTRAINT; Schema: public; Owner: udd
--

ALTER TABLE ONLY public.upload_history_closes
    ADD CONSTRAINT upload_history_closes_source_fkey FOREIGN KEY (source, version) REFERENCES public.upload_history(source, version) DEFERRABLE;


--
-- Name: SCHEMA history; Type: ACL; Schema: -; Owner: udd
--

GRANT USAGE ON SCHEMA history TO PUBLIC;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT USAGE ON SCHEMA public TO PUBLIC;
GRANT ALL ON SCHEMA public TO udd;


--
-- Name: TYPE bugs_severity; Type: ACL; Schema: public; Owner: udd
--

REVOKE ALL ON TYPE public.bugs_severity FROM udd;


--
-- Name: TYPE dehs_status; Type: ACL; Schema: public; Owner: udd
--

REVOKE ALL ON TYPE public.dehs_status FROM udd;


--
-- Name: TYPE ftp_autoreject_level; Type: ACL; Schema: public; Owner: udd
--

REVOKE ALL ON TYPE public.ftp_autoreject_level FROM udd;


--
-- Name: TYPE ftp_autoreject_type; Type: ACL; Schema: public; Owner: udd
--

REVOKE ALL ON TYPE public.ftp_autoreject_type FROM udd;


--
-- Name: TYPE security_issues_releases_status; Type: ACL; Schema: public; Owner: udd
--

REVOKE ALL ON TYPE public.security_issues_releases_status FROM udd;


--
-- Name: TYPE security_issues_scope; Type: ACL; Schema: public; Owner: udd
--

REVOKE ALL ON TYPE public.security_issues_scope FROM udd;


--
-- Name: TABLE sources_count; Type: ACL; Schema: history; Owner: udd
--

GRANT SELECT ON TABLE history.sources_count TO PUBLIC;


--
-- Name: TABLE sources; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.sources TO PUBLIC;


--
-- Name: TABLE uploaders; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.uploaders TO PUBLIC;


--
-- Name: TABLE carnivore_keys; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.carnivore_keys TO PUBLIC;


--
-- Name: TABLE carnivore_login; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.carnivore_login TO PUBLIC;


--
-- Name: TABLE active_dds; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.active_dds TO PUBLIC;


--
-- Name: TABLE archived_bugs; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_bugs TO PUBLIC;


--
-- Name: TABLE bugs; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs TO PUBLIC;


--
-- Name: TABLE all_bugs; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.all_bugs TO PUBLIC;


--
-- Name: TABLE derivatives_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.derivatives_packages TO PUBLIC;


--
-- Name: TABLE packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.packages TO PUBLIC;


--
-- Name: TABLE ubuntu_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_packages TO PUBLIC;


--
-- Name: TABLE all_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.all_packages TO PUBLIC;


--
-- Name: TABLE derivatives_packages_distrelcomparch; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.derivatives_packages_distrelcomparch TO PUBLIC;


--
-- Name: TABLE packages_distrelcomparch; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.packages_distrelcomparch TO PUBLIC;


--
-- Name: TABLE ubuntu_packages_distrelcomparch; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_packages_distrelcomparch TO PUBLIC;


--
-- Name: TABLE all_packages_distrelcomparch; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.all_packages_distrelcomparch TO PUBLIC;


--
-- Name: TABLE derivatives_sources; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.derivatives_sources TO PUBLIC;


--
-- Name: TABLE ubuntu_sources; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_sources TO PUBLIC;


--
-- Name: TABLE all_sources; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.all_sources TO PUBLIC;


--
-- Name: TABLE archived_bugs_blockedby; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_bugs_blockedby TO PUBLIC;


--
-- Name: TABLE archived_bugs_blocks; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_bugs_blocks TO PUBLIC;


--
-- Name: TABLE archived_bugs_fixed_in; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_bugs_fixed_in TO PUBLIC;


--
-- Name: TABLE archived_bugs_found_in; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_bugs_found_in TO PUBLIC;


--
-- Name: TABLE archived_bugs_merged_with; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_bugs_merged_with TO PUBLIC;


--
-- Name: TABLE archived_bugs_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_bugs_packages TO PUBLIC;


--
-- Name: TABLE archived_bugs_stamps; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_bugs_stamps TO PUBLIC;


--
-- Name: TABLE archived_bugs_tags; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_bugs_tags TO PUBLIC;


--
-- Name: TABLE archived_descriptions; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_descriptions TO PUBLIC;


--
-- Name: TABLE archived_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_packages TO PUBLIC;


--
-- Name: TABLE archived_packages_distrelcomparch; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_packages_distrelcomparch TO PUBLIC;


--
-- Name: TABLE archived_packages_summary; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_packages_summary TO PUBLIC;


--
-- Name: TABLE archived_sources; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_sources TO PUBLIC;


--
-- Name: TABLE archived_uploaders; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.archived_uploaders TO PUBLIC;


--
-- Name: TABLE bugs_merged_with; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_merged_with TO PUBLIC;


--
-- Name: TABLE bugs_count; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_count TO PUBLIC;


--
-- Name: TABLE migrations; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.migrations TO PUBLIC;


--
-- Name: TABLE orphaned_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.orphaned_packages TO PUBLIC;


--
-- Name: TABLE popcon_src; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.popcon_src TO PUBLIC;


--
-- Name: TABLE sources_uniq; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.sources_uniq TO PUBLIC;


--
-- Name: TABLE upload_history; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.upload_history TO PUBLIC;


--
-- Name: TABLE upload_history_nmus; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.upload_history_nmus TO PUBLIC;


--
-- Name: TABLE bapase; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bapase TO PUBLIC;


--
-- Name: TABLE bibref; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bibref TO PUBLIC;


--
-- Name: TABLE blends_dependencies; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.blends_dependencies TO PUBLIC;


--
-- Name: TABLE blends_dependencies_alternatives; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.blends_dependencies_alternatives TO PUBLIC;


--
-- Name: TABLE blends_dependencies_priorities; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.blends_dependencies_priorities TO PUBLIC;


--
-- Name: TABLE blends_metadata; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.blends_metadata TO PUBLIC;


--
-- Name: TABLE blends_prospectivepackages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.blends_prospectivepackages TO PUBLIC;


--
-- Name: TABLE blends_remarks; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.blends_remarks TO PUBLIC;


--
-- Name: TABLE blends_tasks; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.blends_tasks TO PUBLIC;


--
-- Name: TABLE blends_unknown_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.blends_unknown_packages TO PUBLIC;


--
-- Name: TABLE bts_tags; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bts_tags TO PUBLIC;


--
-- Name: TABLE bugs_blockedby; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_blockedby TO PUBLIC;


--
-- Name: TABLE bugs_blocks; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_blocks TO PUBLIC;


--
-- Name: TABLE bugs_fixed_in; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_fixed_in TO PUBLIC;


--
-- Name: TABLE bugs_found_in; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_found_in TO PUBLIC;


--
-- Name: TABLE bugs_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_packages TO PUBLIC;


--
-- Name: TABLE bugs_rt_affects_oldstable; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_rt_affects_oldstable TO PUBLIC;


--
-- Name: TABLE bugs_rt_affects_stable; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_rt_affects_stable TO PUBLIC;


--
-- Name: TABLE bugs_rt_affects_testing; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_rt_affects_testing TO PUBLIC;


--
-- Name: TABLE bugs_rt_affects_unstable; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_rt_affects_unstable TO PUBLIC;


--
-- Name: TABLE bugs_rt_affects_testing_and_unstable; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_rt_affects_testing_and_unstable TO PUBLIC;


--
-- Name: TABLE bugs_stamps; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_stamps TO PUBLIC;


--
-- Name: TABLE bugs_tags; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_tags TO PUBLIC;


--
-- Name: TABLE bugs_usertags; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.bugs_usertags TO PUBLIC;


--
-- Name: TABLE carnivore_emails; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.carnivore_emails TO PUBLIC;


--
-- Name: TABLE carnivore_names; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.carnivore_names TO PUBLIC;


--
-- Name: TABLE ci; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ci TO PUBLIC;


--
-- Name: TABLE debian_maintainers; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.debian_maintainers TO PUBLIC;


--
-- Name: TABLE debtags; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.debtags TO PUBLIC;


--
-- Name: TABLE deferred; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.deferred TO PUBLIC;


--
-- Name: TABLE deferred_architecture; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.deferred_architecture TO PUBLIC;


--
-- Name: TABLE deferred_binary; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.deferred_binary TO PUBLIC;


--
-- Name: TABLE deferred_closes; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.deferred_closes TO PUBLIC;


--
-- Name: TABLE dehs; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.dehs TO PUBLIC;


--
-- Name: TABLE derivatives_descriptions; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.derivatives_descriptions TO PUBLIC;


--
-- Name: TABLE derivatives_packages_summary; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.derivatives_packages_summary TO PUBLIC;


--
-- Name: TABLE derivatives_uploaders; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.derivatives_uploaders TO PUBLIC;


--
-- Name: TABLE description_imports; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.description_imports TO PUBLIC;


--
-- Name: TABLE descriptions; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.descriptions TO PUBLIC;


--
-- Name: TABLE duck; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.duck TO PUBLIC;


--
-- Name: TABLE edam; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.edam TO PUBLIC;


--
-- Name: TABLE ftp_autorejects; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ftp_autorejects TO PUBLIC;


--
-- Name: TABLE hints; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.hints TO PUBLIC;


--
-- Name: TABLE key_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.key_packages TO PUBLIC;


--
-- Name: TABLE ldap; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ldap TO guestdd;


--
-- Name: TABLE lintian; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.lintian TO PUBLIC;


--
-- Name: TABLE mentors_raw_uploads; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.mentors_raw_uploads TO PUBLIC;


--
-- Name: TABLE mentors_most_recent_package_versions; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.mentors_most_recent_package_versions TO PUBLIC;


--
-- Name: TABLE migration_excuses; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.migration_excuses TO PUBLIC;


--
-- Name: TABLE new_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.new_packages TO PUBLIC;


--
-- Name: TABLE new_packages_madison; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.new_packages_madison TO PUBLIC;


--
-- Name: TABLE new_sources; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.new_sources TO PUBLIC;


--
-- Name: TABLE new_sources_madison; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.new_sources_madison TO PUBLIC;


--
-- Name: TABLE package_removal; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.package_removal TO PUBLIC;


--
-- Name: TABLE package_removal_batch; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.package_removal_batch TO PUBLIC;


--
-- Name: TABLE packages_summary; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.packages_summary TO PUBLIC;


--
-- Name: TABLE piuparts_status; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.piuparts_status TO PUBLIC;


--
-- Name: TABLE popcon; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.popcon TO PUBLIC;


--
-- Name: TABLE popcon_src_average; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.popcon_src_average TO PUBLIC;


--
-- Name: TABLE potential_bug_closures; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.potential_bug_closures TO PUBLIC;


--
-- Name: TABLE pseudo_packages; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.pseudo_packages TO PUBLIC;


--
-- Name: TABLE registry; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.registry TO PUBLIC;


--
-- Name: TABLE releases; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.releases TO PUBLIC;


--
-- Name: TABLE relevant_hints; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.relevant_hints TO PUBLIC;


--
-- Name: TABLE reproducible; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.reproducible TO PUBLIC;


--
-- Name: TABLE screenshots; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.screenshots TO PUBLIC;


--
-- Name: TABLE security_issues; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.security_issues TO PUBLIC;


--
-- Name: TABLE security_issues_releases; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.security_issues_releases TO PUBLIC;


--
-- Name: TABLE sources_popcon; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.sources_popcon TO PUBLIC;


--
-- Name: TABLE sources_redundant; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.sources_redundant TO PUBLIC;


--
-- Name: TABLE sponsorship_requests; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.sponsorship_requests TO PUBLIC;


--
-- Name: TABLE testing_autoremovals; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.testing_autoremovals TO PUBLIC;


--
-- Name: TABLE timestamps; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.timestamps TO PUBLIC;


--
-- Name: SEQUENCE timestamps_id_seq; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON SEQUENCE public.timestamps_id_seq TO PUBLIC;


--
-- Name: TABLE ubuntu_bugs; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_bugs TO PUBLIC;


--
-- Name: TABLE ubuntu_bugs_duplicates; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_bugs_duplicates TO PUBLIC;


--
-- Name: TABLE ubuntu_bugs_subscribers; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_bugs_subscribers TO PUBLIC;


--
-- Name: TABLE ubuntu_bugs_tags; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_bugs_tags TO PUBLIC;


--
-- Name: TABLE ubuntu_bugs_tasks; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_bugs_tasks TO PUBLIC;


--
-- Name: TABLE ubuntu_description_imports; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_description_imports TO PUBLIC;


--
-- Name: TABLE ubuntu_descriptions; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_descriptions TO PUBLIC;


--
-- Name: TABLE ubuntu_packages_summary; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_packages_summary TO PUBLIC;


--
-- Name: TABLE ubuntu_popcon; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_popcon TO PUBLIC;


--
-- Name: TABLE ubuntu_popcon_src; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_popcon_src TO PUBLIC;


--
-- Name: TABLE ubuntu_popcon_src_average; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_popcon_src_average TO PUBLIC;


--
-- Name: TABLE ubuntu_sources_popcon; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_sources_popcon TO PUBLIC;


--
-- Name: TABLE ubuntu_upload_history; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_upload_history TO PUBLIC;


--
-- Name: TABLE ubuntu_upload_history_closes; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_upload_history_closes TO PUBLIC;


--
-- Name: TABLE ubuntu_upload_history_launchpad_closes; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_upload_history_launchpad_closes TO PUBLIC;


--
-- Name: TABLE ubuntu_uploaders; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.ubuntu_uploaders TO PUBLIC;


--
-- Name: TABLE udd_logs; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.udd_logs TO PUBLIC;


--
-- Name: TABLE upload_history_architecture; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.upload_history_architecture TO PUBLIC;


--
-- Name: TABLE upload_history_closes; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.upload_history_closes TO PUBLIC;


--
-- Name: TABLE upstream; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.upstream TO PUBLIC;


--
-- Name: TABLE upstream_metadata; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.upstream_metadata TO PUBLIC;


--
-- Name: TABLE upstream_status; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.upstream_status TO PUBLIC;


--
-- Name: TABLE vcs; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.vcs TO PUBLIC;


--
-- Name: TABLE vcswatch; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.vcswatch TO PUBLIC;


--
-- Name: TABLE wannabuild; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.wannabuild TO PUBLIC;


--
-- Name: TABLE wnpp; Type: ACL; Schema: public; Owner: udd
--

GRANT SELECT ON TABLE public.wnpp TO PUBLIC;


--
-- PostgreSQL database dump complete
--

